Feature: Scrubber General Logic_229 When L34017 applies to the claim and <code 1> 92225 or 92226 is entered and RT, LT or 50 Mod is not entered.

  Scenario: Logic 229 Case1 When CPT <code 1> 92225 or 92226 is entered and Mod RT, LT or 50 Mod is not entered.
    Given Logic 229 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic229 case1
	Then I should validate following for Logic229 case1
		| row | code | severity | message |
		| 1 | 92225 | MEDIUM | Append modifier RT or LT to 92225 to indicate eye involved. To report services on both eyes, report 92225 on two separate lines and append modifier LT to one and RT to the other (LCD L34017, coding guidelines attachment).|