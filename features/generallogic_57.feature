Feature: Scrubber General Logic_57 When same code for same DOS is entered multiple times without a required modifier.

  Scenario: Logic 57 Case1 When  diff code for same DOS is entered multiple times without a required modifier.
    Given Logic 57 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic57 case1
	Then I should validate following for Logic57 case1
		| row | code | severity | message |
		| 1 | 88304 | MEDIUM | Duplicate code: Append modifier 91 to second and subsequent code for repeated lab test on single DOS. Per CPT&reg;, do not report 91 for tests repeated to confirm results, due to specimen/equipment testing problems, or because code definition require serial measurements.|
		
  Scenario: Logic 57 Case2 When same code for same DOS is entered multiple times without a required modifier.
	Given Logic 57 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic57 case2
	Then I should validate following for Logic57 case2
		| row | code | severity | message |
		| 1 | 88302| MEDIUM | Duplicate code: Append modifier 91 to second and subsequent code for repeated lab test on single DOS. Per CPT&reg;, do not report 91 for tests repeated to confirm results, due to specimen/equipment testing problems, or because code definition require serial measurements.|
		
  Scenario: Logic 57 Case3 When same code for same DOS is entered twice with a required modifier on LX1
	Given Logic 57 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic57 case3
	Then I should validate following for Logic57 case3
		| row | code | severity | message |
		| 1 | 88304| MEDIUM | Duplicate code: Append modifier 91 to second and subsequent code for repeated lab test on single DOS. Per CPT&reg;, do not report 91 for tests repeated to confirm results, due to specimen/equipment testing problems, or because code definition require serial measurements.|
		