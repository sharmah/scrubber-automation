Feature: Scrubber General Logic_256 When LCD L33944 applies to the claim and CPT Codes 15820- 15823 is entered with or without 67900-67908 and 67909-67924 and an ICD-10 code NOT from Group 1 ICD-10 codes that supports Medical Necessity is entered.

  Scenario: Logic 256 Case1 When CPT Codes 15820 is entered with or without 67900-67908 and 67909-67924 and an ICD-10 code Z41.1 is NOT from Group 1 ICD-10 codes
    Given Logic 256 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic256 case1
	Then I should validate following for Logic256 case1
		| row | code | severity | message |
		| 1 | Z41.1 | CRITICAL | Confirm ICD-10 code choice. CGS doesn’t identify Z41.1 in its list of codes that support medical necessity (LCD L33944). [dev should|
		
  Scenario: Logic 256 Case2 When CPT Codes 15820 is entered with or without 67900-67908 and 67909-67924 and an ICD-10 code Z44.21 is NOT from Group 1 ICD-10 codes.
    Given Logic 256 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic256 case2
	Then I should validate following for Logic256 case2
		| row | code | severity | message |
		| 1 | Z44.21 | CRITICAL | Confirm ICD-10 code choice. CGS doesn’t identify Z44.21 in its list of codes that support medical necessity (LCD L33944). [dev should|