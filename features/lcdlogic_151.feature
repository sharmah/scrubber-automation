Feature: Scrubber General Logic_151  When LCD L33628 applies to the claim, user enters 67228 and POS is NOT 11, 21, 22, 23, 24, 49.

  Scenario: Logic 151 Case1 When LCD L33628 applies to the claim, user enters 67228 and POS is NOT 11, 21, 22, 23, 24, 49.
    Given Logic 151 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic151 case1
	Then I should validate following for Logic151 case1
		| row | code | severity | message |
		| 1 | 19 | CRITICAL | Confirm POS. WPS does not list POS 19 as a payable POS for 92133 (Billing and Coding Guidelines attached to LCD L34760).|
		
  Scenario: Logic 151 Case2 When Billing provider speciality is not 65.
    Given Logic 151 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic151 case2
	Then I should validate following for Logic151 case2
		| row | code | severity | message |
		| 1 | 32 | CRITICAL | Confirm POS. WPS does not list POS 32 as a payable POS for 92133 (Billing and Coding Guidelines attached to LCD L34760).|