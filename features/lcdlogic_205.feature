Feature: Scrubber General Logic_205 When LCD L33777 applies to the claim and User enters <code 1> which is any code in Group 6 codes under CPT/HCPCS code in LCD L33777. 

  Scenario: Logic 205 Case1 When CPT 90621 (Group6) is entered.
    Given Logic 205 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic205 case1
	Then I should validate following for Logic205 case1
		| row | code | severity | message |
		| 1 | 90621 | CRITICAL | First Coast indicates 90621 is a non-covered service (LCD L33777).|
