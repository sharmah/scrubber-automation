Feature: Scrubber General Logic_31 When Gender not entered in the claim
  
  Scenario: Logic 31 Case1 Gender not entered in the claim
	Given Logic 31 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic31 case1
	Then I should validate following for Logic31 case1
		| row | code | severity | message |
		| 1 |  | CRITICAL | Gender not selected|