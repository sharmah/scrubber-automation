Feature: Scrubber General Logic_259 When LCD L33954 applies to the claim and CPT is other than 66840, 66850, 66852, 66920 or 66930 and ICD-10 diagnosis from range H26.491 – H26.493 is linked to <CPT code> 

  Scenario: Logic 259 Case1 When CPT 66982 is entered with ICD10 H26.491
    Given Logic 259 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic259 case1
	Then I should validate following for Logic259 case1
		| row | code | severity | message |
		| 1 | 66982| CRITICAL | Confirm code choice. CGS allows H26.491-H26.493 only with 66840, 66850, 66852, 66920, and 66930 and not with 66982, per an instructional note in LCD L33954.|
		
  Scenario: Logic 259 Case2 When CPT 66850 is entered with ICD H26.491
    Given Logic 259 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic259 case2
	Then I should validate following for Logic259 case2
		| row | code | severity | message |
		| 1 | 66850 | CRITICAL | Confirm code choice. CGS allows H26.491-H26.493 only with 66840, 66850, 66852, 66920, and 66930 and not with 66850, per an instructional note in LCD L33954.|