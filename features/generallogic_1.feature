Feature: Scrubber General Logics

  Scenario: Logic 1 Case1 Invalid Diagnosis code
	Given Logic 1 Case1 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case1
    Then I should validate following for logic1 case1
		| row | code | severity | message |
    	| 1 | 420.5 | CRITICAL | Invalid diagnosis code|
		
   Scenario: Logic 1 Case2 Valid Diagnosis code
	Given Logic 1 Case2 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case2
    Then I should validate following for logic1 case2
		| row | code | severity | message |
    	| 1 | G44.1 | CRITICAL | Invalid diagnosis code|
		
  Scenario: Logic 1 Case3 Invalid/valid Diagnosis code
	Given Logic 1 Case3 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case3
    Then I should validate following for logic1 case3
		| row | code | severity | message |
    	| 1 | G44.1 | LOW | An Excludes1 note with G44 restricts you from reporting the following along with G44.1. Note: headache NOS (R51). An interim Excludes1 rule allows you to report both G44.1 and an excluded condition, but only if the two conditions are unrelated.|
		| 1 | 420.5 | CRITICAL | Invalid diagnosis code|
		
  Scenario: Logic 1 Case4 Invalid Diagnosis code - Valid ICD9
	Given Logic 1 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic1 case4		
	Then I should validate following for logic1 case4
		| row | code | severity | message |
    	| 1 | 786.50 | CRITICAL | Invalid diagnosis code|
		
   Scenario: Logic 1 Case5 Valid Diagnosis code- DOS prior Oct, 2015
	Given Logic 1 Case5 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case5
    Then I should validate following for logic1 case5
		| row | code | severity | message |
    	| 1 | G44.1 | CRITICAL | Invalid diagnosis code|
		
  Scenario: Logic 1 Case6 Invalid Diagnosis code - Valid ICD9, DOS prior Oct, 15
	Given Logic 1 Case6 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case6
    Then I should validate following for logic1 case6
		| row | code | severity | message |
    	| 1 | 786.50 | CRITICAL | Invalid diagnosis code|
		
  Scenario: Logic 1 Case7 Invalid Diagnosis code - code with X at 4th place
	Given Logic 1 Case7 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case7
    Then I should validate following for logic1 case7
		| row | code | severity | message |
    	| 1 | C60.X | CRITICAL | Invalid diagnosis code|
		
  Scenario: Logic 1 Case8 Invalid Diagnosis code - PDx- Invalid and SDx- Valid
	Given Logic 1 Case8 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case8
    Then I should validate following for logic1 case8
		| row | code | severity | message |
    	| 1 | 786.50 | CRITICAL | Invalid diagnosis code|
		
   Scenario: Logic 1 Case9 Invalid Diagnosis code - code with X at 6th place
	Given Logic 1 Case9 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case9
    Then I should validate following for logic1 case9
		| row | code | severity | message |
    	| 1 | H40.00X | CRITICAL | Invalid diagnosis code|
		
  Scenario: Logic 1 Case10 Invalid Diagnosis code - code with X at 5th place
	Given Logic 1 Case10 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case10
    Then I should validate following for logic1 case10
		| row | code | severity | message |
    	| 1 | H40.0X1 | CRITICAL | Invalid diagnosis code|
		
  Scenario: Logic 1 Case11 Valid Diagnosis code
	Given Logic 1 Case11 valid EDI
    When I hit the API with username and password with valid EDI for logic1 case11
    Then I should validate following for logic1 case11
		| row | code | severity | message |
    	| 1 | H40.001 | CRITICAL | Invalid diagnosis code|