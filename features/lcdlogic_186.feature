Feature: Scrubber General Logic_186 When LCD L33999 applies to the claim and user enters 76514 (Without modifier 26 or TC) and POS <#> is NOT code 11, 13, 20, 32, 33 or 49.

  Scenario: Logic 186 Case1 When CPT 76514, MOD 26 and TC not entered. POS entered is 11.
    Given Logic 186 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic186 case1
	Then I should validate following for Logic186 case1
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm POS. CGS does not list POS<#> as a payable for POS for global 76514 (CGS Article A52383).|
		
  Scenario: Logic 186 Case2 When CPT 76514, MOD 26 and TC not entered. POS entered is 18.
    Given Logic 186 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic186 case2
	Then I should validate following for Logic186 case2
		| row | code | severity | message |
		| 1 | 15822 | CRITICAL | Confirm POS. CGS does not list POS<#> as a payable for POS for global 76514 (CGS Article A52383).|