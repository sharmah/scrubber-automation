Feature: Scrubber General Logic_227 When L34017 applies to the claim AND code1 92226 AND code2 92002 or 92004.

  Scenario: Logic 227 Case1 When CPT code1 92226 AND code2 92002 or 92004 is entered.
    Given Logic 227 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic227 case1
	Then I should validate following for Logic227 case1
		| row | code | severity | message |
		| 1 | 92226 | CRITICAL | FCSO does not consider 92226 payable with 92226 (LCD L34017, coding guidelines attachment).|