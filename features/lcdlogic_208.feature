Feature: Scrubber General Logic_208 when LCD L33777 applies to the claim and User enters <code 1> which is any code Px code- 44799, 97139, or L8699 Or 84999, 86849, 88749, or 89240 Or J3490 Or 01999, 22899, 27599, 30999, 33477, 33999, 42299, 43289, 43659, 45999, 46601, 46607, 47399, 53899, 58999, 64999, 65785, 66999, 67299, 68399, 68899, 76499, 78699, 78999, 92700, 93050, 93799, 94799, 95199, 95999, 97039, 97799, 99199, 0358T, or 0398T

  Scenario: Logic 208 Case1 When CPT 67299 is entered.
    Given Logic 208 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic208 case1
	Then I should validate following for Logic208 case1
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | 67299 is an unlisted procedure code. The payer will review supporting documentation for services billed with an unlisted procedure code (LCD L33777).|