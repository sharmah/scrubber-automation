Feature: Scrubber General Logic_135 When LCD L34194 applies to the claim and user enters 15820 or 15821

  Scenario: Logic 135 Case1 When LCD L34194 applies to the claim and user enters 15820 or 15821.
    Given Logic 135 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic135 case1
	Then I should validate following for Logic135 case1
		| row | code | severity | message |
		| 1 | 15820 | CRITICAL | Noridian considers lower eyelid blepharoplasty to be non-covered under LCD L34194. You may appeal on a case-by-case basis.|
		
  Scenario: Logic 135 Case2 When LCD L34194 applies to the claim and user enters 15820 or 15821.
    Given Logic 135 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic135 case2
	Then I should validate following for Logic135 case2
		| row | code | severity | message |
		| 1 | 15821 | CRITICAL | Noridian considers lower eyelid blepharoplasty to be non-covered under LCD L34194. You may appeal on a case-by-case basis.|