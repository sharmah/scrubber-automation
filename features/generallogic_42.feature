Feature: Scrubber General Logic_42 Mod 74 is used with the Px
  
  Scenario: Logic 42 Case1 Mod 74 and 50 is used with the Px
	Given Logic 42 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic42 case1
	Then I should validate following for Logic42 case1
		| row | code | severity | message |
		| 1 | 67343 | CRITICAL | Modifier 74 is for use by the outpatient hospital/ASC facility. Physicians should see modifiers 52 and 53 for reduced/discontinued procedures.|
		
  Scenario: Logic 42 Case2 Mod 74 is used with the Px
	Given Logic 42 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic42 case2
	Then I should validate following for Logic42 case2
		| row | code | severity | message |
		| 1 | 67343 | CRITICAL | Modifier 74 is for use by the outpatient hospital/ASC facility. Physicians should see modifiers 52 and 53 for reduced/discontinued procedures.|
		
  Scenario: Logic 42 Case3 Mod 74 is used with the Px
	Given Logic 42 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic42 case3
	Then I should validate following for Logic42 case3
		| row | code | severity | message |
		| 1 | 67343 | CRITICAL | Modifier 74 is for use by the outpatient hospital/ASC facility. Physicians should see modifiers 52 and 53 for reduced/discontinued procedures.|