Feature: Scrubber General Logic_34 Mod 26 entered is entered with CPT having PC/TC indicator either 0 or 2 or 3 or 4 or 5 or 7.
  
  Scenario: Logic 34 Mod 26 is entered with CPT having PC/TC indicator as 1.
	Given Logic 34 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic34 case1
	Then I should validate following for Logic34 case1
		| row | code | severity | message |
		| 1 | 76516 | CRITICAL | Modifier 26 cannot be billed with this procedure code|
		
  Scenario: Logic 34 Case2 Mod 26 entered is entered with CPT having PC/TC indicator either 0 or 2 or 3 or 4 or 5 or 7.
	Given Logic 34 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic34 case2
	Then I should validate following for Logic34 case2
		| row | code | severity | message |
		| 1 | 63308 | CRITICAL | Modifier 26 cannot be billed with this procedure code|
		
  Scenario: Logic 34 Case3 Mod 26 entered is entered with CPT having PC/TC indicator as 9
	Given Logic 34 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic34 case3
	Then I should validate following for Logic34 case3
		| row | code | severity | message |
		| 1 | C2644 | CRITICAL | Modifier 26 cannot be billed with this procedure code|