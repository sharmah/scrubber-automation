Feature: Scrubber General Logic_143 When L35008 applies to the claim and modifier not append  GX and/or GY to <Group 3 CPT code>"

  Scenario: Logic 143 Case1 When L35008 applies to the claim and modifier not append  GX and/or GY to <Group 3 CPT code>"
    Given Logic 143 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic143 case1
	Then I should validate following for Logic143 case1
		| row | code | severity | message |
		| 1 | 0403T | CRITICAL | Confirm whether documentation supports appending GX or GY to 0403T. Noridian indicates it is a "Statutorily Non-covered Service, the Patient is Liable for Payment" (LCD L35008)|
		
  Scenario: Logic 143 Case2 When L35008 applies to the claim and modifier append  GX and/or GY to <Group 3 CPT code>"
    Given Logic 143 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic143 case2
	Then I should validate following for Logic143 case2
		| row | code | severity | message |
		| 1 | 0403T | CRITICAL | Confirm whether documentation supports appending GX or GY to 0403T. Noridian indicates it is a "Statutorily Non-covered Service, the Patient is Liable for Payment" (LCD L35008)|