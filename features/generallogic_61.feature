Feature: Scrubber General Logic_61 When DOS is prior to DOB

  Scenario: Logic 61 Case1 When DOS is same as DOB
    Given Logic 61 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic61 case1
	Then I should validate following for Logic61 case1
		| row | code | severity | message |
		| 1 | 99213 | CRITICAL | Date of Service of the procedure cannot be prior to Date Of Birth|
		
  Scenario: Logic 61 Case2 When DOS is prior to DOB
	Given Logic 61 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic61 case2
	Then I should validate following for Logic61 case2
		| row | code | severity | message |
		| 1 | 99213| CRITICAL | Date of Service of the procedure cannot be prior to Date Of Birth|
		
Scenario: Logic 61 Case3 When DOB is prior to DOS
	Given Logic 61 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic61 case3
	Then I should validate following for Logic61 case3
		| row | code | severity | message |
		| 1 | 99213| CRITICAL | Date of Service of the procedure cannot be prior to Date Of Birth|