Feature: Scrubber General Logic_16 Code1 and code2 lie in some specific range.
  
  Scenario: Logic 16 Case1 Code1 and code2 lie in some specific range.
	Given Logic 16 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic16 case1
	Then I should validate following for logic16 case1
		| row | code | severity | message |
		| 1 | 99202 | MEDIUM | Use modifier 25 if 99202 represents a significant and separately identifiable E/M service beyond 99381|
		
  Scenario: Logic 16 Case2 Code1 and code2 lie in some specific range. Mod 25 appended.
	Given Logic 16 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic16 case2
	Then I should validate following for logic16 case2
		| row | code | severity | message |
		| 1 | 99202 | MEDIUM | Use modifier 25 if 99202 represents a significant and separately identifiable E/M service beyond 99381|	
		
  Scenario: Logic 16 Case3 Code1 and code2 lie in some specific range. Mod other than 25 is appended.
	Given Logic 16 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic16 case3
	Then I should validate following for logic16 case3
		| row | code | severity | message |
		| 1 | 92014 | MEDIUM | Use modifier 25 if 92014 represents a significant and separately identifiable E/M service beyond 99393|