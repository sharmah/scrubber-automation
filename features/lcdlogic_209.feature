Feature: Scrubber General Logic_209 When LCD L33777 applies to the claim and User enters <code 1>  E1399 and mod. Code GY with E1399.

  Scenario: Logic 209 Case1 When CPT E1399 is entered without Mod GY.
    Given Logic 209 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic209 case1
	Then I should validate following for Logic209 case1
		| row | code | severity | message |
		| 1 | E1399 | CRITICAL | Appending GY indicates the payer does not cover E1399 (L33777). Expect a denial.|
		
  Scenario: Logic 209 Case2 When CPT E1399 is entered with Mod GY
    Given Logic 209 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic209 case2
	Then I should validate following for Logic209 case2
		| row | code | severity | message |
		| 1 | E1399 | CRITICAL | Appending GY indicates the payer does not cover E1399 (L33777). Expect a denial.|