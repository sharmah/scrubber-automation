Feature: Scrubber General Logic_81 When LCD L33377 is applied and patient DOB on DOS is less than 65 years.

  Scenario: Logic 81 Case1 When LCD L33377 is applied and patient DOB on DOS is greater than 65 years.
    Given Logic 81 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic81 case1
	Then I should validate following for Logic81 case1
		| row | code | severity | message |
		| 1 | 0308T | CRITICAL | Confirm patient DOB. “The intraocular telescope is indicated for monocular implantation to improve vision in patients who are greater than or equal to 65 years of age” (LCD L33377).|
		
  Scenario: Logic 81 Case2 When LCD L33377 is applied and patient DOB on DOS is less than 65 years.
	Given Logic 81 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic81 case2
	Then I should validate following for Logic81 case2
		| row | code | severity | message |
		| 1 | 0308T | CRITICAL | Confirm patient DOB. “The intraocular telescope is indicated for monocular implantation to improve vision in patients who are greater than or equal to 65 years of age” (LCD L33377).|
		
  Scenario: Logic 81 Case3 When LCD L33377 is applied and patient DOB on DOS is less than 65 years.
    Given Logic 81 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic81 case3
	Then I should validate following for Logic81 case3
		| row | code | severity | message |
		| 1 | 0308T | CRITICAL | Confirm patient DOB. “The intraocular telescope is indicated for monocular implantation to improve vision in patients who are greater than or equal to 65 years of age” (LCD L33377).|