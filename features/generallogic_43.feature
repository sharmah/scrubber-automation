Feature: Scrubber General Logic_43 Mod 51 is used with the CPT have MOD 51 expected or is an Addon code
  
  Scenario: Logic 43 Case1 Mod 51 is used with CPT 17004 having Mod 51 exempted
	Given Logic 43 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic43 case1
	Then I should validate following for Logic43 case1
		| row | code | severity | message |
		| 1 | 17004 | MEDIUM | Invalid modifier: Modifier 51 exempted from use with this code.|
		
  Scenario: Logic 43 Case2 Mod 51 is used with CPT 67225 having Mod 51 exempted
	Given Logic 43 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic43 case2
	Then I should validate following for Logic43 case2
		| row | code | severity | message |
		| 1 | 67225 | MEDIUM | Invalid modifier: Modifier 51 exempted from use with this code.|
		
  Scenario: Logic 43 Case3 Mod 51 is used with the Px not having Mod 51 exempted
	Given Logic 43 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic43 case3
	Then I should validate following for Logic43 case3
		| row | code | severity | message |
		| 1 | 73560 | MEDIUM | Invalid modifier: Modifier 51 exempted from use with this code.|
		
  Scenario: Logic 43 Case4 Mod 51 is used with the Px not having Mod 51 exempted
	Given Logic 43 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic43 case4
	Then I should validate following for Logic43 case4
		| row | code | severity | message |
		| 1 | 67335 | MEDIUM | Invalid modifier: Modifier 51 exempted from use with this code.|
		
  Scenario: Logic 43 Case5 Mod 51 is used with the Px not having Mod 51 exempted
	Given Logic 43 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic43 case5
	Then I should validate following for Logic43 case5
		| row | code | severity | message |
		| 1 | 67221 | MEDIUM | Invalid modifier: Modifier 51 exempted from use with this code.|