Feature: Scrubber General Logic_47 When +99100 is entered without Anesthesia code 00100-01999.

  Scenario: Logic 47 Case1 When +99100 is entered without Anesthesia code 00100-01999.
    Given Logic 47 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic47 case1
	Then I should validate following for Logic47 case1
		| row | code | severity | message |
		| 1 | 99100 | MEDIUM | +99100 cannot be billed without any primary anesthesia code|
		
  Scenario: Logic 47 Case2 Anesthesia code 00140 is entered with +99100.
	Given Logic 47 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic47 case2
	Then I should validate following for Logic47 case2
		| row | code | severity | message |
		| 1 | 99100 | MEDIUM | +99100 cannot be billed without any primary anesthesia code|
		
  Scenario: Logic 47 Case3 Code +99100 is entered with code 65930 and without Anesthesia code 00100-01999.
	Given Logic 47 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic47 case3
	Then I should validate following for Logic47 case3
		| row | code | severity | message |
		| 1 | 99100 | MEDIUM | +99100 cannot be billed without any primary anesthesia code|
		
Scenario: Logic 47 Case4 Anesthesia code 00326 is entered with +99100.
	Given Logic 47 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic47 case4
	Then I should validate following for Logic47 case4
		| row | code | severity | message |
		| 1 | 99100 | MEDIUM | +99100 cannot be billed without any primary anesthesia code|
		
Scenario: Logic 47 Case5 Code +99100 is entered without Anesthesia code 00100-01999.
	Given Logic 47 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic47 case5
	Then I should validate following for Logic47 case5
		| row | code | severity | message |
		| 1 | 99100 | MEDIUM | +99100 cannot be billed without any primary anesthesia code|