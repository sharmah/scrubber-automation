Feature: Scrubber General Logic_181 when LCD L36232 applies to the claim and User enters 0330T and ICD-10 code linked to 0330T is from Group 1 ICD-10 codes under ICD-10 Codes that Support Medical Necessity in L36232.


  Scenario: Logic 181 Case1 When CPT - 0330T is used with Dx M35.01 (Group1)
    Given Logic 181 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic181 case1
	Then I should validate following for Logic181 case1
		| row | code | severity | message |
		| 1 | 0330T | CRITICAL | First Coast does not cover 0330T for dry eyes/sicca syndrome (LCD L36232).|
		
  Scenario: Logic 181 Case2 When CPT - 0330T is used with Dx A18.59 (not from Group1)
    Given Logic 181 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic181 case2
	Then I should validate following for Logic181 case2
		| row | code | severity | message |
		| 1 | 0330T | CRITICAL | First Coast does not cover 0330T for dry eyes/sicca syndrome (LCD L36232).|