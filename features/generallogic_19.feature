Feature: Scrubber General Logic_19 Code1 and code2 lie in some specific range.
  
  Scenario: Logic 19 Case1 Code1 and code2 lie in some specific range 
	Given Logic 19 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic19 case1
	Then I should validate following for Logic19 case1
		| row | code | severity | message |
		| 1 | 99203 | MEDIUM | Use modifier 57 if 99203 represents a significant and separately identifiable E/M service above and beyond the usual preoperative and postoperative care associated with  13160|
		
  Scenario: Logic 19 Case2 Code1 and code2 lie in some specific range.
	Given Logic 19 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic19 case2
	Then I should validate following for Logic19 case2
		| row | code | severity | message |
		| 1 | 99490 | MEDIUM | Use modifier 57 if 99490 represents a significant and separately identifiable E/M service above and beyond the usual preoperative and postoperative care associated with  13160|
		
  Scenario: Logic 19 Case3 Code1 and code2 lie in some specific range. Mod 57 appended
	Given Logic 19 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic19 case3
	Then I should validate following for Logic19 case3
		| row | code | severity | message |
		| 1 | 99203 | MEDIUM | Use modifier 57 if 99203 represents a significant and separately identifiable E/M service above and beyond the usual preoperative and postoperative care associated with 13160|
		
  Scenario: Logic 19 Case4 Code1 and code2 lie in some specific range. Mod 57 appended
	Given Logic 19 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic19 case4
	Then I should validate following for Logic19 case4
		| row | code | severity | message |
		| 1 | 99490 | MEDIUM | Use modifier 57 if 99490 represents a significant and separately identifiable E/M service above and beyond the usual preoperative and postoperative care associated with 13160|