Feature: Scrubber General Logic_154 hen L34760 applies to the claim and <code 1> 92250, 92225, 92226, or 76512 and <code 2> 92132, 92133, or 92134 are entered on the same DOS.

  Scenario: Logic 154 Case1 When code1 and code2 are on same claim and have same DOS.
    Given Logic 154 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic154 case1
	Then I should validate following for Logic154 case1
		| row | code | severity | message |
		| 1 | 92250 | MEDIUM | WPS generally does not consider 92250 to be necessary with 92132. Documentation must justify medical necessity for procedures performed on the same DOS (LCD L34760).|
		
  Scenario: Logic 154 Case2 When code1 and code2 are on same claim and have different DOS.
    Given Logic 154 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic154 case2
	Then I should validate following for Logic154 case2
		| row | code | severity | message |
		| 1 | 92250 | MEDIUM | WPS generally does not consider 92250 to be necessary with 92132. Documentation must justify medical necessity for procedures performed on the same DOS (LCD L34760).|