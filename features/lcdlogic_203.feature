Feature: Scrubber General Logic_203 When LCD L33777 applies to the claim and User enters <code 1> which is any code in Group 4 codes under CPT/HCPCS code in LCD L33777. 

  Scenario: Logic 203 Case1 When CPT 81504 (Group4) is entered.
    Given Logic 203 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic203 case1
	Then I should validate following for Logic203 case1
		| row | code | severity | message |
		| 1 | 81504 | CRITICAL | First Coast indicates 81504 is a non-covered service (LCD L33777).|
		
  Scenario: Logic 203 Case2 When CPT 67299 (not from group4) is entered
    Given Logic 203 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic203 case2
	Then I should validate following for Logic203 case2
		| row | code | severity | message |
		| 1 | 67299 | CRITICAL | First Coast indicates 67299 is a non-covered service (LCD L33777).|