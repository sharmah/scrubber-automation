Feature: Scrubber General Logic_237 When L33927 applies to the claim and single claim contains two claim lines for CPT code 92136 and Mod 26 and donot append modifier (LT & RT or 50) to one or both units (i.e LT with one unit & RT with one unit of 92136).

  Scenario: Logic 237 Case1 When CPT code 92136 and Mod 26 and donot append modifier (LT & RT or 50) and units entered is 2.
    Given Logic 237 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic237 case1
	Then I should validate following for Logic237 case1
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Use modifiers LT & RT or 50 to indicate services performed bilaterally. Code 92136-26 with 2 units on same DOS is considered a Bilateral service (per Coding Guidelines attached with LCD L33927).|
		
  Scenario: Logic 237 Case2 When CPT code 92136 and Mod 26 and append modifier (LT & RT or 50) and units entered is 2.
    Given Logic 237 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic237 case2
	Then I should validate following for Logic237 case2
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Use modifiers LT & RT or 50 to indicate services performed bilaterally. Code 92136-26 with 2 units on same DOS is considered a Bilateral service (per Coding Guidelines attached with LCD L33927).|