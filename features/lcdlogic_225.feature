Feature: Scrubber General Logic_225 When LCD L33751 applies to the claim and the user enters CPT code1 92250 and code2 92133 or 92134 on same DOS for same eye identify using Modifier RT/LT .

  Scenario: Logic 225 Case1 When CPT code1 92250 and code2 92133 or 92134 on same DOS for same eye identify using Modifier RT on both .
    Given Logic 225 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic225 case1
	Then I should validate following for Logic225 case1
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | CPT 92250 is bundled into CPT 92250. You may override the edit with a modifier 59 or XU on CPT 92250 if circumstances and documentation support 92133 being distinct from 92250.|
		
  Scenario: Logic 225 Case2 When CPT code1 92133 or 92134 and code2 92250 on same DOS for same eye identify using Modifier LT on both .
    Given Logic 225 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic225 case2
	Then I should validate following for Logic225 case2
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | CPT 92250 is bundled into CPT 92250. You may override the edit with a modifier 59 or XU on CPT 92250 if circumstances and documentation support 92133 being distinct from 92250.|
		
  Scenario: Logic 225 Case3 When CPT code1 92250 and code2 92133 or 92134 on same DOS for same eye identify using Modifier RT and LT resp.
    Given Logic 225 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic225 case3
	Then I should validate following for Logic225 case3
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | CPT 92250 is bundled into CPT 92250. You may override the edit with a modifier 59 or XU on CPT 92250 if circumstances and documentation support 92133 being distinct from 92250.|
		
  Scenario: Logic 225 Case4 When CPT code1 92250 and code2 92133 or 92134 on same DOS for same eye identify using Modifier RT and LT resp.
    Given Logic 225 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic225 case4
	Then I should validate following for Logic225 case4
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | CPT 92250 is bundled into CPT 92250. You may override the edit with a modifier 59 or XU on CPT 92250 if circumstances and documentation support 92133 being distinct from 92250.|