Feature: Scrubber General Logic_90  When  <ICD-10 code> specific group linked to <CPT code> (which is either 92225, 92226, 92228, or 92250) and L33467 applies to the claim.

  Scenario: Logic 90 Case1 When A18.50 linked to 92225 and L33467 applies to the claim.
    Given Logic 90 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic90 case1
	Then I should validate following for Logic90 case1
		| row | code | severity | message |
		| 1 | A18.50 | CRITICAL | Confirm ICD-10 code choice. Palmetto does not list A18.50 as a code that supports medical necessity for 92225 (LCD L33467).|
		
  Scenario: Logic 90 Case2 When only Mod RT is associated.
    Given Logic 90 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic90 case2
	Then I should validate following for Logic90 case2
		| row | code | severity | message |
		| 1 | A18.53 | CRITICAL | Confirm ICD-10 code choice. Palmetto does not list A18.53 as a code that supports medical necessity for 92225 (LCD L33467).|