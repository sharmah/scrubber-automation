Feature: Scrubber General Logic_163 When LCD L34171 applies to the claim having CPT <code> 68801, 68810 to 68815, or 68840 and <code> have no modifier from this list: LT, RT, 50

  Scenario: Logic 163 Case1 When no modifier is used.
    Given Logic 163 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic163 case1
	Then I should validate following for Logic163 case1
		| row | code | severity | message |
		| 1 | 68801 | CRITICAL | You must append LT, RT, or 50 to 68801 For a bilateral service, append 50, not LT/RT (CGS Article A52391).|
		
  Scenario: Logic 163 Case2 When POS entered is 32
    Given Logic 163 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic163 case2
	Then I should validate following for Logic163 case2
		| row | code | severity | message |
		| 1 | 68801 | CRITICAL | You must append LT, RT, or 50 to 68801 For a bilateral service, append 50, not LT/RT (CGS Article A52391).|