Feature: Scrubber General Logic_55 When 2E/M codes are mentioned without mod 25 for same DOS.

  Scenario: Logic 55 Case1 2E/M codes are mentioned without mod 25 for same DOS.
    Given Logic 55 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic55 case1
	Then I should validate following for Logic55 case1
		| row | code | severity | message |
		| 1 | 99241 | MEDIUM | Multiple E/M services cannot be reported for single patient on single DOS by same physician or by multiple physicians of same group and specialty. Exception: Additional E/M services for unrelated problems may be reported separately with modifier 25. Exception doesn’t apply to 'per day' codes or services that could have been provided at same encounter. Payer rules may vary. (Medicare Claims Processing Manual, Chap. 12, Section 30.6)|
		
  Scenario: Logic 55 Case2 2E/M codes are mentioned with mod 25 on one of the code for same DOS. 
	Given Logic 55 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic55 case2
	Then I should validate following for Logic55 case2
		| row | code | severity | message |
		| 1 | 99212| MEDIUM | Multiple E/M services cannot be reported for single patient on single DOS by same physician or by multiple physicians of same group and specialty. Exception: Additional E/M services for unrelated problems may be reported separately with modifier 25. Exception doesn’t apply to 'per day' codes or services that could have been provided at same encounter. Payer rules may vary. (Medicare Claims Processing Manual, Chap. 12, Section 30.6)|