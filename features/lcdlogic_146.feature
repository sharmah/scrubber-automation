Feature: Scrubber General Logic_146 When L34512 applies to the claim having CPT 76514 and Billing Provider Specialty is not equal to 18 or 41.

  Scenario: Logic 146 Case1 When L34512 applies to the claim having CPT 76514 and Billing Provider Specialty is not equal to 18 or 41.
    Given Logic 146 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic146 case1
	Then I should validate following for Logic146 case1
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm provider billing specialty. Palmetto doesn’t allow billing provider specialties other than 18 (Ophthalmology) and 41 (Optometry) for 76514 (LCD L34512).|
		
  Scenario: Logic 146 Case2 When L34512 applies to the claim having CPT 76514 and Billing Provider Specialty is not equal to 18.
    Given Logic 146 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic146 case2
	Then I should validate following for Logic146 case2
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm provider billing specialty. Palmetto doesn’t allow billing provider specialties other than 18 (Ophthalmology) and 41 (Optometry) for 76514 (LCD L34512).|
		
  Scenario: Logic 146 Case3 When L34512 applies to the claim having CPT 76514 and Billing Provider Specialty is not equal to 41.
    Given Logic 146 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic146 case3
	Then I should validate following for Logic146 case3
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm provider billing specialty. Palmetto doesn’t allow billing provider specialties other than 18 (Ophthalmology) and 41 (Optometry) for 76514 (LCD L34512).|