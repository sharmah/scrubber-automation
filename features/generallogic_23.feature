Feature: Scrubber General Logic_23 Two E/M codes are used on same DOS without Mod 25 or 57
  
  Scenario: Logic 23 Case1 Two E/M codes are used on same DOS without Mod 25 or 57
	Given Logic 23 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic23 case1
	Then I should validate following for Logic23 case1
		| row | code | severity | message |
		| 1 | 99213 | CRITICAL | Modifier 25 or 57 required for same day E/M service|
		
  Scenario: Logic 23 Case2 Two E/M codes are used on same DOS with Mod 25 or 57 
	Given Logic 23 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic23 case2
	Then I should validate following for Logic23 case2
		| row | code | severity | message |
		| 1 | 99213 | CRITICAL | Modifier 25 or 57 required for same day E/M service|