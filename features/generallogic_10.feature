Feature: Scrubber General Logic_10 Procedure and age conflict
  
  Scenario: Logic 10 Case1 Procedure and age conflict 'where age required less than 29 days'
	Given Logic 10 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic10 case1
	Then I should validate following for logic10 case1
		| row | code | severity | message |
		| 1 | 99471 | MEDIUM | Procedure and age conflict|
		
  Scenario: Logic 10 Case2 Procedure and age conflict 'Age entered is less than 29 days'
	Given Logic 10 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic10 case2
	Then I should validate following for logic10 case2
		| row | code | severity | message |
		| 1 | 99471 | MEDIUM | Procedure and age conflict|
		
  Scenario: Logic 10 Case3 Procedure and age conflict 'where age required is min 40 years'
	Given Logic 10 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic10 case3
	Then I should validate following for logic10 case3
		| row | code | severity | message |
		| 1 | 99396 | MEDIUM | Procedure and age conflict|
		
  Scenario: Logic 10 Case4 Procedure and age conflict 'where age required is min 28 days'
	Given Logic 10 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic10 case4
	Then I should validate following for logic10 case4
		| row | code | severity | message |
		| 1 | 99469 | MEDIUM | Procedure and age conflict|