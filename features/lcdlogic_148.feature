Feature: Scrubber General Logic_148  When LCD L35098 applies to the claim User enters CPT 95905 and dx other than G56.01 or G56.02, which are ICD-10 codes listed in coverage section for code 95905.

  Scenario: Logic 148 Case1 When LCD L35098 applies to the claim User enters CPT 95905 and dx G56.01 or G56.02, which are ICD-10 codes listed in coverage section for code 95905.
    Given Logic 148 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic148 case1
	Then I should validate following for Logic148 case1
		| row | code | severity | message |
		| 1 | G56.01 | CRITICAL | Confirm ICD-10 diagnosis code choice. NGS doesn’t list G56.01 as a diagnosis code that supports medical necessity for 95905. NGS covers G56.01 and G56.02 for 95905 (LCD L35098).|
		
  Scenario: Logic 148 Case2 When LCD L35098 applies to the claim User enters CPT 95905 and dx other than G56.01 or G56.02, which are ICD-10 codes listed in coverage section for code 95905.
    Given Logic 148 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic148 case2
	Then I should validate following for Logic148 case2
		| row | code | severity | message |
		| 1 | C72.31 | CRITICAL | Confirm ICD-10 diagnosis code choice. NGS doesn’t list C72.31 as a diagnosis code that supports medical necessity for 95905. NGS covers G56.01 and G56.02 for 95905 (LCD L35098).|