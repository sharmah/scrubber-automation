Feature: Scrubber General Logic_32 Modifier other than 24, 25, 57, GC entered against E/M code.
  
  Scenario: Logic 32 Case1 Modifier other than 24, 25, 57, GC entered against E/M code. 
	Given Logic 32 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic32 case1
	Then I should validate following for Logic32 case1
		| row | code | severity | message |
		| 1 | 99202 | CRITICAL | Invalid modifier: Modifier AF can be used only with Non E/M service codes|
		
  Scenario: Logic 32 Case2 Valid Modifier entered against E/M code.
	Given Logic 32 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic32 case2
	Then I should validate following for Logic32 case2
		| row | code | severity | message |
		| 1 | 99221 | CRITICAL | Invalid modifier: Modifier AI can be used only with Non E/M service codes|
		
  Scenario: Logic 32 Case3 Modifier 24 is associated to a CPT code and E/M code both.
	Given Logic 32 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic32 case2
	Then I should validate following for Logic32 case2
		| row | code | severity | message |
		| 1 | 99221 | CRITICAL | Invalid modifier: Modifier 80 can be used only with Non E/M service codes|