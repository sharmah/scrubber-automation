Feature: Scrubber General Logic_233 When L33810 applies to claim and CPT <code1> is 92025 and Dx is not equal to H11.001 - H11.069, H11.141- H11.149, H17.89, H17.9, H18.10-H18.13, H18.451-H18.469, H18.59, H18.601-H18.629, H18.711-H18.719, H53.2, T85.318.A-T85.318S, T85.328A-T85.328S, T85.398A-T85.398S, T86.840, T86.841

  Scenario: Logic 233 Case1 When CPT 92025 is entered with Dx H27.111 (not in the list)
    Given Logic 233 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case1
	Then I should validate following for Logic56 case1
		| row | code | severity | message |
		| 1 | H27.111 | CRITICAL | Diagnosis code H27.111 is not under LCD (Policy # L33810) coverage for this state and contractor|
		
  Scenario: Logic 233 Case2 When CPT 92025 is entered with Dx H53.2 (from the list)
	Given Logic 233 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case2
	Then I should validate following for Logic56 case2
		| row | code | severity | message |
		| 1 | H53.2| CRITICAL | Diagnosis code H53.2 is not under LCD (Policy # L33810) coverage for this state and contractor|