Feature: Scrubber General Logic_178 When LCD L34528 applies to the claim and user enters CPT 15822 or 15823 and user enters ICD-10 Dx Z41.1 linked to <code 1> And Modifier GY is not linked with <code 1>

  Scenario: Logic 178 Case1 When CPT 15822, Dx Z411 is entered and No MOD GY is associated.
    Given Logic 178 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic178 case1
	Then I should validate following for Logic178 case1
		| row | code | severity | message |
		| 1 | 15822 | CRITICAL | Medicare considers eyelid blepharoplasty for cosmetic reasons to be a non-covered service. Append modifier GY to 15822 (Billing & Coding Guidelines with LCD L34528).|
		
  Scenario: Logic 178 Case2 When CPT 15822, Dx Z411 is entered and MOD GY is associated.
    Given Logic 178 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic178 case2
	Then I should validate following for Logic178 case2
		| row | code | severity | message |
		| 1 | 15822 | CRITICAL | Medicare considers eyelid blepharoplasty for cosmetic reasons to be a non-covered service. Append modifier GY to 15822 (Billing & Coding Guidelines with LCD L34528).|
		
  Scenario: Logic 178 Case3 When CPT 15822, Dx is not Z411 is entered and MOD GY is not associated.
    Given Logic 178 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic178 case3
	Then I should validate following for Logic178 case3
		| row | code | severity | message |
		| 1 | 15822 | CRITICAL | Medicare considers eyelid blepharoplasty for cosmetic reasons to be a non-covered service. Append modifier GY to 15822 (Billing & Coding Guidelines with LCD L34528).|