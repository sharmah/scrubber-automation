Feature: Scrubber General Logic_136 When LCD L35094 applies to the claim and User enters 0100T"

  Scenario: Logic 136 Case1 When LCD L35094 applies to the claim and User enters 0100T"
    Given Logic 136 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic136 case1
	Then I should validate following for Logic136 case1
		| row | code | severity | message |
		| 1 | 11 | LOW | Policy notice: Code 0100T is subject to the Humanitarian Device Exemption (HDE) rule (LCD L35094). Please refer to the Novitas Solutions website under Medical Policy - LCDs/Clinical Trials and Devices.|