Feature: Scrubber General Logic_8 Invalid Procedure Code
  
  Scenario: Logic 8 Case1 Invalid Procedure Code 'CPT'
	Given Logic 8 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic8 case1
	Then I should validate following for logic8 case1
		| row | code | severity | message |
		| 1 | 11102 | CRITICAL | Invalid procedure code|
		
  Scenario: Logic 8 Case2 Valid Procedure Code 'CPT'
	Given Logic 8 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic8 case2
	Then I should validate following for logic8 case2
		| row | code | severity | message |
		| 1 | 11201 | CRITICAL | Invalid procedure code|
		
  Scenario: Logic 8 Case3 Valid Procedure Code 'HCPCS'
	Given Logic 8 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic8 case3
	Then I should validate following for logic8 case3
		| row | code | severity | message |
		| 1 | J0586 | CRITICAL | Invalid procedure code|

  Scenario: Logic 8 Case4 Invalid Procedure Code having less than 5 digits
	Given Logic 8 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic8 case4
	Then I should validate following for logic8 case4
		| row | code | severity | message |
		| 1 | 9201 | CRITICAL | Invalid procedure code|
		
  Scenario: Logic 8 Case5 Valid Procedure Code having 'T' at end
	Given Logic 8 Case5 valid EDI
	When I hit the API with username and password with valid EDI for logic8 case5
	Then I should validate following for logic8 case5
		| row | code | severity | message |
		| 1 | 0042T | CRITICAL | Invalid procedure code|
		
  Scenario: Logic 8 Case6 Valid Procedure Code having 'F' at end
	Given Logic 8 Case6 valid EDI
	When I hit the API with username and password with valid EDI for logic8 case6
	Then I should validate following for logic8 case6
		| row | code | severity | message |
		| 1 | 0500F | CRITICAL | Invalid procedure code|