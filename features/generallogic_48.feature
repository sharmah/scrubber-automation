Feature: Scrubber General Logic_48 When Add-on code is used without a primary Px code.

  Scenario: Logic 48 Case1 When Add-on code is used without a primary Px code.
    Given Logic 48 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic48 case1
	Then I should validate following for Logic48 case1
		| row | code | severity | message |
		| 1 | 22208 | CRITICAL | Add-on code, cannot be billed without a primary procedure code|
		
  Scenario: Logic 48 Case2 When Add-on code is used without a primary Px code but with some other primary code
    Given Logic 48 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic48 case2
	Then I should validate following for Logic48 case2
		| row | code | severity | message |
		| 1 | 22208 | CRITICAL | Add-on code, cannot be billed without a primary procedure code|