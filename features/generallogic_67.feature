Feature: Scrubber General Logic_67 When modifier 59 is entered against a procedure code

  Scenario: Logic 67 Case1 When modifier 59 is entered against a procedure code
    Given Logic 67 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic67 case1
	Then I should validate following for Logic67 case1
		| row | code | severity | message |
		| 1 | 12031 | LOW | Use precise alternative to modifier 59: You may want to report modifiers XE/XP/XS/XU replacing 59 in specific situations such as separate encounter, separate practitioner, separate site or unusual non-overlapping service. Modifier 59 should not be used when a more descriptive modifier is available. CMS will continue to recognize the modifier 59 in many instances but may selectively require a more specific -X(EPSU) modifier for billing certain codes at high risk for incorrect billing.|
		
  Scenario: Logic 67 Case2 When modifier 59 is entered against a procedure code
	Given Logic 67 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic67 case2
	Then I should validate following for Logic67 case2
		| row | code | severity | message |
		| 1 | 12031 | LOW | Use precise alternative to modifier 59: You may want to report modifiers XE/XP/XS/XU replacing 59 in specific situations such as separate encounter, separate practitioner, separate site or unusual non-overlapping service. Modifier 59 should not be used when a more descriptive modifier is available. CMS will continue to recognize the modifier 59 in many instances but may selectively require a more specific -X(EPSU) modifier for billing certain codes at high risk for incorrect billing.|
		
  Scenario: Logic 67 Case4 When modifier 59 is entered against a procedure code
    Given Logic 67 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic67 case4
	Then I should validate following for Logic67 case4
		| row | code | severity | message |
		| 1 | 71021 | LOW | Use precise alternative to modifier 59: You may want to report modifiers XE/XP/XS/XU replacing 59 in specific situations such as separate encounter, separate practitioner, separate site or unusual non-overlapping service. Modifier 59 should not be used when a more descriptive modifier is available. CMS will continue to recognize the modifier 59 in many instances but may selectively require a more specific -X(EPSU) modifier for billing certain codes at high risk for incorrect billing.|