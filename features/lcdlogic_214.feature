Feature: Scrubber General Logic_214 when the user enters CPT <Code 2> that is listed for the LCD L34061 for CPT<Code 1> for same DOS and for same eye.

  Scenario: Logic 214 Case1 When CPT code1 92133 and Code2 92250 are entered on same DOS.
    Given Logic 214 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic214 case1
	Then I should validate following for Logic214 case1
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | Check for the necessity of the procedures. CPT 92133 would not be necessary with CPT 92250. You may require to check the documentation, must justify the procedures.|
		
  Scenario: Logic 214 Case2 When CPT code1 92133 and Code2 76512 are entered on different DOS.
    Given Logic 214 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic214 case2
	Then I should validate following for Logic214 case2
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | Check for the necessity of the procedures. CPT 92133 would not be necessary with CPT 76512. You may require to check the documentation, must justify the procedures.|