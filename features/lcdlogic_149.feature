Feature: Scrubber General Logic_149 When LCD L33628 applies to the claim and User enters CPT 67228 without modifier RT or LT or 50 appended.

  Scenario: Logic 149 Case1 When LCD L33628 applies to the claim and User enters CPT 67228 without modifier RT or LT or 50 appended.
    Given Logic 149 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic149 case1
	Then I should validate following for Logic149 case1
		| row | code | severity | message |
		| 1 | 67228 | CRITICAL | Append modifier RT or LT to 67228 to identify the eye involved. Append 50 for a bilateral service (NGS Article A52822).|
		
  Scenario: Logic 149 Case2 When LCD L33628 applies to the claim and User enters CPT 67228 with modifier RT/LT
    Given Logic 149 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic149 case2
	Then I should validate following for Logic149 case2
		| row | code | severity | message |
		| 1 | 67228 | CRITICAL | Append modifier RT or LT to 67228 to identify the eye involved. Append 50 for a bilateral service (NGS Article A52822).|
		
  Scenario: Logic 149 Case3 When LCD L33628 applies to the claim and User enters CPT 67228 with modifier 50
    Given Logic 149 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic149 case3
	Then I should validate following for Logic149 case3
		| row | code | severity | message |
		| 1 | 67228 | CRITICAL | Append modifier RT or LT to 67228 to identify the eye involved. Append 50 for a bilateral service (NGS Article A52822).|