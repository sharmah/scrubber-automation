Feature: Scrubber General Logic_188 When LCD L33999 applies to the claim and user enters  76514-26 and POS <#> is NOT 11, 13, 20, 21, 22, 23, 24, 31, 32, 33, or 49 

  Scenario: Logic 188 Case1 When CPT 76514-26, POS 19 is entered.
    Given Logic 188 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic188 case1
	Then I should validate following for Logic188 case1
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm POS. CGS does not list POS <#> as a payable POS for 76514-26 (CGS Article A52383).|
		
  Scenario: Logic 188 Case2 When CPT 76514-26, POS 49 is entered.
    Given Logic 188 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic188 case2
	Then I should validate following for Logic188 case2
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm POS. CGS does not list POS <#> as a payable POS for 76514-26 (CGS Article A52383).|