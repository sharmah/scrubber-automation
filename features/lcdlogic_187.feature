Feature: Scrubber General Logic_187 When LCD L33999 applies to the claim and user enters  76514-TC and POS <#> is NOT 11, 13, 20, 32, 33, 49, 50 or 72.

  Scenario: Logic 187 Case1 When CPT 76514-TC and POS 32 is entered.
    Given Logic 187 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic187 case1
	Then I should validate following for Logic187 case1
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm POS. CGS does not list POS <#> as a payable POS for 76514-TC (CGS Article A52383).|
		
  Scenario: Logic 187 Case2 When CPT 76514-TC and POS 19 is entered.
    Given Logic 187 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic187 case2
	Then I should validate following for Logic187 case2
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm POS. CGS does not list POS <#> as a payable POS for 76514-TC (CGS Article A52383).|