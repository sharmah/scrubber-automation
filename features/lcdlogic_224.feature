Feature: Scrubber General Logic_224 When LCD L33670 applies to the claim and the user enters CPT code1 92250 and code2 92133 or 92134 and ICD listed in last column on same DOS for same eye identify using Modifier RT/LT/50 or If  cannot ID eye for one or both codes.

  Scenario: Logic 224 Case1 When CPT code1 92250 and code2 92133 or 92134 is entered, ICD used is B58.01 (not from the list)
    Given Logic 224 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic224 case1
	Then I should validate following for Logic224 case1
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | CPT 92250 is bundled into CPT 92133. You may override the edit with a modifier 59 or XU on CPT 92250 if circumstances and documentation support 92250 being distinct from 92133.|
		
  Scenario: Logic 224 Case2 When CPT code1 92250 and code2 92133 or 92134 is entered have diff DOS, ICD used is B58.01 (not from the list).
    Given Logic 224 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic224 case2
	Then I should validate following for Logic224 case2
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | CPT 92250 is bundled into CPT 92133. You may override the edit with a modifier 59 or XU on CPT 92250 if circumstances and documentation support 92250 being distinct from 92133.|
		
  Scenario: Logic 224 Case3 When CPT code1 92250 and code2 92133 or 92134 is entered on same DOS, ICD used is B39.4 (from the list).
    Given Logic 224 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic224 case3
	Then I should validate following for Logic224 case3
		| row | code | severity | message |
		| 1 | 92133 | MEDIUM | CPT 92250 is bundled into CPT 92133. You may override the edit with a modifier 59 or XU on CPT 92250 if circumstances and documentation support 92250 being distinct from 92133.|