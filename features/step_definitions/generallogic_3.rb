$response = ""

Given (/^Logic 3 Case1 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case1.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case1$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case1$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case2 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case2.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case2$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case2$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case3 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case3.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case3$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case3$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case4 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case4.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case4$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case4$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case5 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case5.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case5$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case5$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case6 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case6.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case6$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case6$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case7 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case7.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case7$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case7$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case8 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case8.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case8$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case8$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case9 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case9.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case9$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case9$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case10 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case10.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case10$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case10$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case11 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case11.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case11$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case11$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 3 Case12 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic3\\Logic3_Case12.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic3 case12$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic3 case12$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end