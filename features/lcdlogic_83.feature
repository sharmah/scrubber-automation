Feature: Scrubber General Logic_83 When CPT 0100T is used with LCD L33392.

  Scenario: Logic 83 Case1 When CPT 0100T is used with LCD L33392.
    Given Logic 83 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic83 case1
	Then I should validate following for Logic83 case1
		| row | code | severity | message |
		| 1 | 19 | LOW | NGS allows this service for the FDA-approved indications. If NGS denies the procedure as not medically necessary, it will also deny the device (LCD L33392)|