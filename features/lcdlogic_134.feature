Feature: Scrubber General Logic_134  When LCD L34615 applies to the claim and code 1 is either 92081-26, 92082-26, or 92083-26 AND POS <#> is NOT 11, 21, 22, 23, 31, 32, 49, or 62.

  Scenario: Logic 134 Case1 When code 92082-26 and POS 13 is entered.
    Given Logic 134 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic134 case1
	Then I should validate following for Logic134 case1
		| row | code | severity | message |
		| 1 | 13 | CRITICAL | Confirm POS. WPS does not list POS 13 as a payable POS for 92082 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 134 Case2 When code 92082-26 and POS 11 is entered.
    Given Logic 134 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic134 case2
	Then I should validate following for Logic134 case2
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | Confirm POS. WPS does not list POS 11 as a payable POS for 92082 (LCD L34615, Billing and Coding Guidelines attachment).|