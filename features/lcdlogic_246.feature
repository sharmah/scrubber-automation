Feature: Scrubber General Logic_246 When LCD L34594 applies to the claim and User links CPT 95905 to <ICD-10 code> not G56.01 or G56.02 and listed in coverage section for code 95905.

  Scenario: Logic 246 Case1 When User links CPT 95905 to <ICD-10 code> not G56.01 or G56.02
    Given Logic 246 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic246 case1
	Then I should validate following for Logic246 case1
		| row | code | severity | message |
		| 1 | B91 | CRITICAL | Confirm ICD-10 diagnosis code choice. WPS doesn’t list B91 as a diagnosis code that supports medical necessity for 95905 (LCD L34594).|
		
  Scenario: Logic 246 Case2 When User links CPT 95905 to <ICD-10 code>  G56.01 or G56.02
    Given Logic 246 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic246 case2
	Then I should validate following for Logic246 case2
		| row | code | severity | message |
		| 1 | G56.02 | CRITICAL | Confirm ICD-10 diagnosis code choice. WPS doesn’t list G56.02 as a diagnosis code that supports medical necessity for 95905 (LCD L34594).|