Feature: Scrubber General Logic_183 When LCD L36232 applies to the claim and User enters 68761 and user appends more than one modifier from this list to 68761 on a single line item: E1, E2, E3, E4.


  Scenario: Logic 183 Case1 When CPT - 68761 and Mod E1 and E2 both are entered.
    Given Logic 183 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic183 case1
	Then I should validate following for Logic183 case1
		| row | code | severity | message |
		| 1 | 68761 | CRITICAL | To report insertion of multiple plugs, report each punctum as a separate line item of 68761 with a single modifier from E1-E4 on each line (First Coast Article A54680).|
		
  Scenario: Logic 183 Case2 When two claim lines of same CPT is entered with Mod E1 on one line and Mod E2 on second.
    Given Logic 183 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic183 case2
	Then I should validate following for Logic183 case2
		| row | code | severity | message |
		| 1 | 68761 | CRITICAL | To report insertion of multiple plugs, report each punctum as a separate line item of 68761 with a single modifier from E1-E4 on each line (First Coast Article A54680).|