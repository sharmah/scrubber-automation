Feature: Scrubber General Logic_63 When DOS is prior to the effective date of the Px code.

  Scenario: Logic 63 Case1 When DOS is after the effective date of the Px code.
    Given Logic 63 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic63 case1
	Then I should validate following for Logic63 case1
		| row | code | severity | message |
		| 1 | 0099T | CRITICAL | Procedure code is not valid and existing on this DOS|
		
  Scenario: Logic 63 Case2 When DOS is prior to the effective date of the Px code.
	Given Logic 63 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic63 case2
	Then I should validate following for Logic63 case2
		| row | code | severity | message |
		| 1 | 0099T| CRITICAL | Procedure code is not valid and existing on this DOS|
		
  Scenario: Logic 63 Case3 When DOS is after the effective date of the Px code.
    Given Logic 63 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic63 case3
	Then I should validate following for Logic63 case3
		| row | code | severity | message |
		| 1 | 65785 | CRITICAL | Procedure code is not valid and existing on this DOS|
		
  Scenario: Logic 63 Case4 When When DOS is prior to the effective date of the Px code.
	Given Logic 63 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic63 case4
	Then I should validate following for Logic63 case4
		| row | code | severity | message |
		| 1 | 65785| CRITICAL | Procedure code is not valid and existing on this DOS|

