Feature: Scrubber General Logic_45 When +99100 is entered with Anesthesia code 00326, 00561, 00834, 00836 .
  
  Scenario: Logic 45 Case1 Anesthesia code 00834 is entered without +99100.
	Given Logic 45 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic45 case1
	Then I should validate following for Logic45 case1
		| row | code | severity | message |
		| 1 | 00834 | CRITICAL | +99100 cannot be reported with this anesthesia code|
		
  Scenario: Logic 45 Case2 Anesthesia code 00834 is entered with +99100.
	Given Logic 45 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic45 case2
	Then I should validate following for Logic45 case2
		| row | code | severity | message |
		| 1 | 00834 | CRITICAL | +99100 cannot be reported with this anesthesia code|
		
  Scenario: Logic 45 Case3 Code +99100 is entered with Anesthesia code 00836
	Given Logic 45 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic45 case3
	Then I should validate following for Logic45 case3
		| row | code | severity | message |
		| 1 | 00836 | CRITICAL | +99100 cannot be reported with this anesthesia code|
		
  Scenario: Logic 45 Case4 Anesthesia code Px 00561 is entered without +99100 code and age of the patient is 11 months 30 days
	Given Logic 45 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic45 case4
	Then I should validate following for Logic45 case4
		| row | code | severity | message |
		| 1 | 00326 | CRITICAL | +99100 cannot be reported with this anesthesia code|
