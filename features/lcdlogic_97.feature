Feature: Scrubber General Logic_97  When ICD-10 code linked to CPT code(Group1), LCD L33558 applies to the  claim and none of the ICD lie under Group1 ICD-10 Codes

  Scenario: Logic 97 Case1 When ICD-10 H22 linked to CPT 66850(Group1), LCD L33558 applies to the  claim.
    Given Logic 97 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic97 case1
	Then I should validate following for Logic97 case1
		| row | code | severity | message |
		| 1 | H22 | CRITICAL | Confirm diagnosis code choice. NGS does not list H22 as a code that supports medical necessity for 66850 (LCD L33538).|
		
  Scenario: Logic 97 Case2 When ICD-10 H22 linked to CPT 66982(Group1), LCD L33558 applies to the  claim.
    Given Logic 97 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic97 case2
	Then I should validate following for Logic97 case2
		| row | code | severity | message |
		| 1 | H22 | CRITICAL | Confirm diagnosis code choice. NGS does not list H22 as a code that supports medical necessity for 66850 (LCD L33538).|