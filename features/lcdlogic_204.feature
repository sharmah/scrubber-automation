Feature: Scrubber General Logic_204 When LCD L33777 applies to the claim and user enters <Code 1> which is code 87270 or 87320. And Dx Code is not Z11.3.

  Scenario: Logic 204 Case1 When CPT 87270 is entered and DX z11.3 is not associated.
    Given Logic 204 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic204 case1
	Then I should validate following for Logic204 case1
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | First Coast indicates 87270 is not medically reasonable and necessary except when billed with diagnosis Z11.3 (LCD L33777).|
		
  Scenario: Logic 204 Case2 When CPT 87270, DX z11.3 is entered.
    Given Logic 204 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic204 case2
	Then I should validate following for Logic204 case2
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | First Coast indicates 87270 is not medically reasonable and necessary except when billed with diagnosis Z11.3 (LCD L33777).|