Feature: Scrubber General Logic_184 When LCD L33999 applies to the claim and user enters CPT 76514 and user enters modifier RT or LT (not both) appended to 76514 and Modifier 52 is not linked with CPT 76514.

  Scenario: Logic 184 Case1 When CPT 76514, Mod RT is used and Mod 52 is not entered.
    Given Logic 184 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic184 case1
	Then I should validate following for Logic184 case1
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm modifier choice. To report unilateral 76514, report RT or LT along with 52 (CGS Article A52383).|
		
  Scenario: Logic 184 Case2 When CPT 76514, Mod RT and 52 are entered.
    Given Logic 184 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic184 case2
	Then I should validate following for Logic184 case2
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm modifier choice. To report unilateral 76514, report RT or LT along with 52 (CGS Article A52383).|
		
  Scenario: Logic 184 Case3 Case2 When CPT 76514, No Mod RT/LT and 52 are entered.
    Given Logic 184 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic184 case3
	Then I should validate following for Logic184 case3
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm modifier choice. To report unilateral 76514, report RT or LT along with 52 (CGS Article A52383).|