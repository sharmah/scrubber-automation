Feature: Scrubber General Logic_89 When modifier RT or LT and not 52 appended to <code 1>, which is either 92227, 92228, or 92250, when L33467 applies to the claim.

  Scenario: Logic 89 Case1 When no modifier is associated with 92227, 92228, or 92250.
    Given Logic 89 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic89 case1
	Then I should validate following for Logic89 case1
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | Use modifier 52 on 92228 to indicate unilateral service (Article A53060). Palmetto does not require modifier RT or LT for a unilateral service.|
		
  Scenario: Logic 89 Case2 When only Mod RT is associated.
    Given Logic 89 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic89 case2
	Then I should validate following for Logic89 case2
		| row | code | severity | message |
		| 1 | 92228 | CRITICAL | Use modifier 52 on 92228 to indicate unilateral service (Article A53060). Palmetto does not require modifier RT or LT for a unilateral service.|
		
  Scenario: Logic 89 Case3 When both RT or LT and 52 is associated.
    Given Logic 89 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic89 case3
	Then I should validate following for Logic89 case3
		| row | code | severity | message |
		| 1 | 92228 | CRITICAL | Use modifier 52 on 92228 to indicate unilateral service (Article A53060). Palmetto does not require modifier RT or LT for a unilateral service.|
		
  Scenario: Logic 89 Case4 When only 52 is associated.
    Given Logic 89 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic89 case4
	Then I should validate following for Logic89 case4
		| row | code | severity | message |
		| 1 | 92250 | CRITICAL | Use modifier 52 on 92250 to indicate unilateral service (Article A53060). Palmetto does not require modifier RT or LT for a unilateral service.|