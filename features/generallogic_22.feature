Feature: Scrubber General Logic_22 When CPT code having Bilateral Surgery indicator 1 or 3 is entered, Mod 50 is appended and more than 1 unit is entered for CPT.
  
  Scenario: Logic 22 Case1 When CPT code having Bilateral Surgery indicator 1 or 3 is entered, Mod 50 is appended and more than 1 unit is entered for CPT.
	Given Logic 22 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic22 case1
	Then I should validate following for Logic22 case1
		| row | code | severity | message |
		| 1 | 65091 | CRITICAL | Check units reported for 65091. You have already indicated a bilateral service by appending modifier 50.|
		
  Scenario: Logic 22 Case2 When CPT code having Bilateral Surgery indicator 1 or 3 is entered, Mod 50 is appended and 1 unit is entered for CPT.
    Given Logic 22 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic22 case2
	Then I should validate following for Logic22 case2
		| row | code | severity | message |
		| 1 | 65091 | CRITICAL | Check units reported for 65091. You have already indicated a bilateral service by appending modifier 50.|	