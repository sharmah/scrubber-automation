Feature: Scrubber General Logic_138 When LCD L35094 applies to the claim and User enters <code1> belongs to group1 under CPT/HCPCS Codes 

  Scenario: Logic 138 Case1 When LCD L35094 applies to the claim and User enters <code1> belongs to group1 under CPT/HCPCS Codes 
    Given Logic 138 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic138 case1
	Then I should validate following for Logic138 case1
		| row | code | severity | message |
		| 1 | 0207T | CRITICAL | Novitas considers 0207T to be not reasonable and necessary (LCD L35094).|