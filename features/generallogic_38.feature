Feature: Scrubber General Logic_38 Mod 80 is entered and  modifier indicator for that CPT code is either 1 or 9.
  
  Scenario: Logic 38 Mod 80 is entered and  modifier indicator for that CPT code is not 1 or 9.
	Given Logic 38 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic38 case1
	Then I should validate following for Logic38 case1
		| row | code | severity | message |
		| 1 | 67113 | MEDIUM | Modifier 80 cannot be appended to this code: Assistant surgeon not permitted for this procedure|
		
  Scenario: Logic 38 Case2 Mod 80 is entered and  modifier indicator for that CPT code is either 1 or 9.
	Given Logic 38 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic38 case2
	Then I should validate following for Logic38 case2
		| row | code | severity | message |
		| 1 | 66982 | MEDIUM | Modifier 80 cannot be appended to this code: Assistant surgeon not permitted for this procedure|
		
  Scenario: Logic 38 Case3 Mod 80 is entered and  modifier indicator for that CPT code is not 1 or 9.
	Given Logic 38 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic38 case3
	Then I should validate following for Logic38 case3
		| row | code | severity | message |
		| 1 | 70030 | MEDIUM | Modifier 80 cannot be appended to this code: Assistant surgeon not permitted for this procedure|
		
  Scenario: Logic 38 Case4 Mod 80 is entered and  modifier indicator for that CPT code is either 1 or 9.
	Given Logic 38 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic38 case4
	Then I should validate following for Logic38 case4
		| row | code | severity | message |
		| 1 | 99509 | MEDIUM | Modifier 80 cannot be appended to this code: Assistant surgeon not permitted for this procedure|
		