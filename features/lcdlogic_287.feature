Feature: Scrubber General Logic_287 When LCD L33558 applies to the claim and CPT 66982 is entered with PDx from Group 1 ICD-10 codes in L33558 and there is no SDx appended.

  Scenario: Logic 287 Case1 PT 66982 is entered with PDx from Group 1 ICD-10 codes in L33558 and there is SDx as well.
    Given Logic 287 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic287 case1
	Then I should validate following for Logic287 case1
		| row | code | severity | message |
		| 1 |  | CRITICAL | NGS requires an applicable secondary diagnosis code for 66982. See LCD L33538 for allowed codes.|
		
  Scenario: Logic 287 Case2 When CPT 66940 (grp1) is entered, ICD used is E10.36 and the patient is less than 15 years in age on DOS.
    Given Logic 287 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic287 case2
	Then I should validate following for Logic287 case2
		| row | code | severity | message |
		| 1 |  | CRITICAL | NGS requires an applicable secondary diagnosis code for 66982. See LCD L33538 for allowed codes.|