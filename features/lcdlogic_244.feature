Feature: Scrubber General Logic_244 When L33927 applies to the claim and CPT 92136-TC appended with Mod RT or LT (not both).

  Scenario: Logic 244 Case1 When CPT 66940 (grp1) is entered, ICD used is E10.36 and the patient is 15 years or older on DOS.
    Given Logic 244 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic244 case1
	Then I should validate following for Logic244 case1
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Do not bill site modifiers (RT, LT) with 92136-TC. 92136-TC is a bilateral service, and Show message with 92136 when L33927 applies to the claim Demographics, LCD contract # DB applies to 92136-TC-LT reimbursement is based on the total service for both eyes (per Coding Guidelines attached with LCD L33927).|