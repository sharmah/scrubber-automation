Feature: Scrubber General Logic_222 When LCD L34203 applies to the claim CPT entered is GROUP1 CPT under the CPT/HCPCS Codes header in LCD and ICD used is H26.8 and the patient is 15 years or older on DOS.

  Scenario: Logic 222 Case1 When CPT 66982 (grp1) is entered, ICD used is H26.8 and the patient is 15 years or older on DOS.
    Given Logic 222 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic222 case1
	Then I should validate following for Logic222 case1
		| row | code | severity | message |
		| 1 | H26.8 | CRITICAL | In LCD L34203, Noridian states that when billing H26.8, coding guidelines require you to identify the causative agent. The ICD-10-CM Tabular does not contain that instruction. ClaimsEase has contacted Noridian for clarification.|
		
  Scenario: Logic 222 Case2 When CPT 66982 (grp1) is entered, ICD used is H25.012 and the patient is 15 years or older on DOS.
    Given Logic 222 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic222 case2
	Then I should validate following for Logic222 case2
		| row | code | severity | message |
		| 1 | H25.012 | CRITICAL | In LCD L34203, Noridian states that when billing H25.012, coding guidelines require you to identify the causative agent. The ICD-10-CM Tabular does not contain that instruction. ClaimsEase has contacted Noridian for clarification.|
		
  Scenario: Logic 222 Case3 When CPT 66982 (grp1) is entered, ICD used is H26.8 and the patient is less than 15 years in age on DOS.
    Given Logic 222 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic222 case3
	Then I should validate following for Logic222 case3
		| row | code | severity | message |
		| 1 | H26.8 | CRITICAL | In LCD L34203, Noridian states that when billing H26.8, coding guidelines require you to identify the causative agent. The ICD-10-CM Tabular does not contain that instruction. ClaimsEase has contacted Noridian for clarification.|