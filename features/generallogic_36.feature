Feature: Scrubber General Logic_36 Mod 62 is entered and  modifier indicator for that CPT code is either 0 or 9.
  
  Scenario: Logic 36 Mod 62 is entered and  modifier indicator for that CPT code is either 0 or 9.
	Given Logic 36 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic36 case1
	Then I should validate following for Logic36 case1
		| row | code | severity | message |
		| 1 | 65101 | MEDIUM | Modifier 62 cannot be appended to this code: Co-surgeons not permitted for this procedure|
		
  Scenario: Logic 36 Case2 Mod 62 is entered and  modifier indicator for that CPT code is not 0 or 9.
	Given Logic 36 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic36 case2
	Then I should validate following for Logic36 case2
		| row | code | severity | message |
		| 1 | 61522 | MEDIUM | Modifier 62 cannot be appended to this code: Co-surgeons not permitted for this procedure|
		
  Scenario: Logic 36 Case3 Mod 62 and some other Mod is entered and modifier indicator for that CPT code is not 0 or 9.
	Given Logic 36 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic36 case3
	Then I should validate following for Logic36 case3
		| row | code | severity | message |
		| 1 | 65101 | MEDIUM | Modifier 62 cannot be appended to this code: Co-surgeons not permitted for this procedure|