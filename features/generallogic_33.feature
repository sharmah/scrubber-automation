Feature: Scrubber General Logic_33 Mod. 63 is used against a CPT code, which is present at “modifier 63 exempted code” list.
  
  Scenario: Logic 33 Case1 Mod. 63 is used against a CPT code, which is not present at “modifier 63 exempted code” list.
	Given Logic 33 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic33 case1
	Then I should validate following for Logic33 case1
		| row | code | severity | message |
		| 1 | 33692 | CRITICAL | Invalid modifier: Modifier 63 exempted from use with this code.|
		
  Scenario: Logic 33 Case2 Mod. 63 is used against a CPT code, which is present at “modifier 63 exempted code” list.
	Given Logic 33 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic33 case2
	Then I should validate following for Logic33 case2
		| row | code | severity | message |
		| 1 | 33619 | CRITICAL | Invalid modifier: Modifier 63 exempted from use with this code.|