Feature: Scrubber General Logic_64 When Code 2 is included in the global surgical package of Code 1 and the code represents the E/M service 

  Scenario: Logic 64 Case1 When Code 2 is in global surgical period of Code 1 and the code1 is E/M code without mod 57
    Given Logic 64 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic64 case1
	Then I should validate following for Logic64 case1
		| row | code | severity | message |
		| 1 | | LOW | 99203 is included in the global surgical package of 65101 Append modifier 57 to 99203 when the code represents the E/M service at which the provider decided to perform surgery.|
		
  Scenario: Logic 64 Case2 When Code 2 is not in global surgical period of Code 1.
	Given Logic 64 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic64 case2
	Then I should validate following for Logic64 case2
		| row | code | severity | message |
		| 1 | | LOW | 99203 is included in the global surgical package of 65800 Append modifier 57 to 99203 when the code represents the E/M service at which the provider decided to perform surgery.|
		
  Scenario: Logic 64 Case3 When Code 2 is in global surgical period of Code 1, code 1 and code 2 performed on same DOS and the code1 is E/M code without mod 57.
    Given Logic 64 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic64 case3
	Then I should validate following for Logic64 case3
		| row | code | severity | message |
		| 1 | | LOW | 99213 is included in the global surgical package of 66020 Append modifier 57 to 99213 when the code represents the E/M service at which the provider decided to perform surgery.|
		
  Scenario: Logic 64 Case4 When Code 2 is in global surgical period of Code 1 and the code1 is E/M code with mod 57
	Given Logic 64 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic64 case4
	Then I should validate following for Logic64 case4
		| row | code | severity | message |
		| 1 | | LOW | 99203 is included in the global surgical package of 65101 Append modifier 57 to 99203 when the code represents the E/M service at which the provider decided to perform surgery.|
		
  Scenario: Logic 64 Case5 When Code 2 is in global surgical period of Code 1 and the code1 is E/M code with mod 25
	Given Logic 64 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic64 case5
	Then I should validate following for Logic64 case5
		| row | code | severity | message |
		| 1 | | LOW | 99203 is included in the global surgical package of 65101 Append modifier 57 to 99203 when the code represents the E/M service at which the provider decided to perform surgery.|

  Scenario: Logic 64 Case6 When Code 2 is in global surgical period of Code 1 and the code1 is E/M code without mod 57
	Given Logic 64 Case6 valid EDI
	When I hit the API with username and password with valid EDI for Logic64 case6
	Then I should validate following for Logic64 case6
		| row | code | severity | message |
		| 1 | | LOW | 99203 is included in the global surgical package of 65101 Append modifier 57 to 99203 when the code represents the E/M service at which the provider decided to perform surgery.|
