Feature: Scrubber General Logic_9 Deleted Procedure Code
  
  Scenario: Logic 9 Case1 Deleted Procedure Code 'DOS and code deletion date are same.'
	Given Logic 9 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic9 case1
	Then I should validate following for logic9 case1
		| row | code | severity | message |
		| 1 | 21805 | CRITICAL | Deleted procedure code|
		
  Scenario: Logic 9 Case2 Deleted Procedure Code 'DOS is after deletion date'
	Given Logic 9 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic9 case2
	Then I should validate following for logic9 case2
		| row | code | severity | message |
		| 1 | 0073T | CRITICAL | Deleted procedure code|
		
  Scenario: Logic 9 Case3 Deleted Procedure Code 'DOS is before deletion date'
	Given Logic 9 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic9 case3
	Then I should validate following for logic9 case3
		| row | code | severity | message |
		| 1 | 0073T | CRITICAL | Deleted procedure code|
		
  Scenario: Logic 9 Case4 Claim have two CPT, one of them is deleted. Error message should come for only deleted one.
	Given Logic 9 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic9 case4
	Then I should validate following for logic9 case4
		| row | code | severity | message |
		| 1 | 0099T | CRITICAL | Deleted procedure code|