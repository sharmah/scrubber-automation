$response = ""

Given (/^Logic 224 Case1 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic224\\Logic224_Case1.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic224 case1$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic224 case1$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 224 Case2 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic224\\Logic224_Case2.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic224 case2$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic224 case2$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 224 Case3 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic224\\Logic224_Case3.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic224 case3$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic224 case3$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end
