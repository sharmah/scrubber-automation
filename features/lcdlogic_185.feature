Feature: Scrubber General Logic_185 when LCD L33999 applies to the claim and user enters CPT 76514 and user enters modifier 52 appended to 76514 and Modifier RT or LT is not linked with CPT 76514.

  Scenario: Logic 185 Case1 When CPT 76514 and MOD 52 is entered only.
    Given Logic 185 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic185 case1
	Then I should validate following for Logic185 case1
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm modifier choice. To report unilateral 76514, report RT or LT along with 52 (CGS Article A52383).|
		
  Scenario: Logic 185 Case2 When CPT 76514 and MOD RT/LT and 52 are entered.
    Given Logic 185 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic185 case2
	Then I should validate following for Logic185 case2
		| row | code | severity | message |
		| 1 | 76514 | CRITICAL | Confirm modifier choice. To report unilateral 76514, report RT or LT along with 52 (CGS Article A52383).|