Feature: Scrubber General Logic_206 When LCD L33777 applies to the claim and User enters <code 1> which is any code in Group 8 codes under CPT/HCPCS code in LCD L33777. 

  Scenario: Logic 206 Case1 When CPT 15820 (Group8) is entered.
    Given Logic 206 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic206 case1
	Then I should validate following for Logic206 case1
		| row | code | severity | message |
		| 1 | 15820 | CRITICAL | First Coast indicates 15820 is a non-covered service (LCD L33777).|
		
  Scenario: Logic 206 Case2 When CPT 0388T (not from group8) is entered
    Given Logic 206 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic206 case2
	Then I should validate following for Logic206 case2
		| row | code | severity | message |
		| 1 | 0388T | CRITICAL | First Coast indicates 0388T is a non-covered service (LCD L33777).|