Feature: Scrubber General Logic_35 Mod TC entered is entered with CPT having PC/TC indicator either 0 or 2 or 3 or 4 or 5 or 7.
  
  Scenario: Logic 35 Mod TC is entered with CPT having PC/TC indicator is either 0 or 2 or 3 or 4 or 5 or 7.
	Given Logic 35 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic35 case1
	Then I should validate following for Logic35 case1
		| row | code | severity | message |
		| 1 | 65101 | CRITICAL | Modifier TC cannot be billed with this procedure code|
		
  Scenario: Logic 35 Case2 Mod TC entered is entered with CPT having PC/TC indicator as 1
	Given Logic 35 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic35 case2
	Then I should validate following for Logic35 case2
		| row | code | severity | message |
		| 1 | 70030 | CRITICAL | Modifier TC cannot be billed with this procedure code|
		
  Scenario: Logic 35 Case3 Mod TC entered is entered with CPT having PC/TC indicator as 9
	Given Logic 35 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic35 case3
	Then I should validate following for Logic35 case3
		| row | code | severity | message |
		| 1 | V2623 | CRITICAL | Modifier TC cannot be billed with this procedure code|