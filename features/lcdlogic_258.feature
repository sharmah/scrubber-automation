Feature: Scrubber General Logic_258 When LCD L33954 applies to the claim and CPT is other than 66840, 66850, or 66852 and ICD-10 diagnosis from range H59.021 – H59.023is linked to <CPT code> 

  Scenario: Logic 258 Case1 When CPT 66930 is entered, ICD used is H59.021 is entered.
    Given Logic 258 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic258 case1
	Then I should validate following for Logic258 case1
		| row | code | severity | message |
		| 1 | 66930 | CRITICAL | Confirm code choice. CGS allows H59.021-H59.023 only with 66840, 66850, and 66852, and not with 66930, per an instructional note in LCD L33954.|
		
  Scenario: Logic 258 Case2 When CPT 66840 is entered, ICD used is H59.021 is entered.
    Given Logic 258 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic258 case2
	Then I should validate following for Logic258 case2
		| row | code | severity | message |
		| 1 | 66840 | CRITICAL | Confirm code choice. CGS allows H59.021-H59.023 only with 66840, 66850, and 66852, and not with 66930, per an instructional note in LCD L33954.|