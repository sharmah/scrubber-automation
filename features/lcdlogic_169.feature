Feature: Scrubber General Logic_169 When LCD 33968 applies to the claim and user enters CPT 66821 without mod LT, RT or 50 

  Scenario: Logic 169 Case1 When LCD 33968 applies to the claim and user enters CPT 66821 without mod LT, RT or 50 
    Given Logic 169 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic169 case1
	Then I should validate following for Logic169 case1
		| row | code | severity | message |
		| 1 | 66821 | CRITICAL | Append modifier LT, RT, or 50 to 66821 to identify eye(s) involved (FCSO Coding guidelines with L33968).|
		
  Scenario: Logic 169 Case2 When LCD 33968 applies to the claim and user enters CPT 66821 without mod LT, RT or 50 and DOS is before policy effective date.
    Given Logic 169 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic169 case2
	Then I should validate following for Logic169 case2
		| row | code | severity | message |
		| 1 | 66821 | CRITICAL | Append modifier LT, RT, or 50 to 66821 to identify eye(s) involved (FCSO Coding guidelines with L33968).|
		
  Scenario: Logic 169 Case3 When LCD 33968 applies to the claim and user enters CPT 66821 with mod RT.
    Given Logic 169 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic169 case3
	Then I should validate following for Logic169 case3
		| row | code | severity | message |
		| 1 | 66821 | CRITICAL | Append modifier LT, RT, or 50 to 66821 to identify eye(s) involved (FCSO Coding guidelines with L33968).|
		
  Scenario: Logic 169 Case4 When LCD 33968 applies to the claim and user enters CPT 66821 with mod 50.
    Given Logic 169 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic169 case4
	Then I should validate following for Logic169 case4
		| row | code | severity | message |
		| 1 | 66821 | CRITICAL | Append modifier LT, RT, or 50 to 66821 to identify eye(s) involved (FCSO Coding guidelines with L33968).|