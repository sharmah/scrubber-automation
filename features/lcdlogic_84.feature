Feature: Scrubber General Logic_84 when group1 CPT code is used with LCD L33392 in the claim

  Scenario: Logic 84 Case1 When CPT 0100T is used with LCD L33392.
    Given Logic 84 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic84 case1
	Then I should validate following for Logic84 case1
		| row | code | severity | message |
		| 1 | 0051T | CRITICAL | NGS considers 0051T to be not medically necessary (LCD L33392).|