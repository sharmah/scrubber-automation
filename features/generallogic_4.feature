Feature: Scrubber General Logic_4 V00-Y99 is billed as primary diagnosis code
  
  Scenario: Logic 4 Case1 Primary diagnosis code is any code from V00-Y99
	Given Logic 4 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic4 case1
	Then I should validate following for logic4 case1
		| row | code | severity | message |
		| 1 | Y77.2 | CRITICAL | External Cause Diagnosis cannot be a Primary Code. Please enter a valid Primary ICD-10-CM Code.|
		
   Scenario: Logic 4 Case2 Secondary diagnosis code is any code from V00-Y99
	Given Logic 4 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic4 case2
	Then I should validate following for logic4 case2
		| row | code | severity | message |
		| 1 | Y77.2 | CRITICAL | Diagnosis and sex conflict.|