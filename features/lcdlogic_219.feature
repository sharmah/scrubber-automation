Feature: Scrubber General Logic_219 When LCD L34203 applies to the claim CPT entered is GROUP1 CPT under the CPT/HCPCS Codes header in LCD and ICD used is E10.36, E11.36, or E13.36 and the patient is 15 years or older on DOS.

  Scenario: Logic 219 Case1 When CPT 66940 (grp1) is entered, ICD used is E10.36 and the patient is 15 years or older on DOS.
    Given Logic 219 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic219 case1
	Then I should validate following for Logic219 case1
		| row | code | severity | message |
		| 1 | E10.36 | CRITICAL | In LCD L34203, Noridian states that when billing E10.36, coding guidelines require you to sequence the ICD-10 code for the underlying condition first on the claim. The ICD-10-CM Tabular does not contain that instruction. ClaimsEase has contacted Noridian for clarification.|
		
  Scenario: Logic 219 Case2 When CPT 66940 (grp1) is entered, ICD used is E10.36 and the patient is less than 15 years in age on DOS.
    Given Logic 219 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic219 case2
	Then I should validate following for Logic219 case2
		| row | code | severity | message |
		| 1 | E10.36 | CRITICAL | In LCD L34203, Noridian states that when billing E10.36, coding guidelines require you to sequence the ICD-10 code for the underlying condition first on the claim. The ICD-10-CM Tabular does not contain that instruction. ClaimsEase has contacted Noridian for clarification.|
		
  Scenario: Logic 219 Case3 When CPT 66940 (grp1) is entered, ICD used is H25.012 and the patient is 15 years or older on DOS.
    Given Logic 219 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic219 case3
	Then I should validate following for Logic219 case3
		| row | code | severity | message |
		| 1 | H25.012 | CRITICAL | In LCD L34203, Noridian states that when billing H25.012, coding guidelines require you to sequence the ICD-10 code for the underlying condition first on the claim. The ICD-10-CM Tabular does not contain that instruction. ClaimsEase has contacted Noridian for clarification.|