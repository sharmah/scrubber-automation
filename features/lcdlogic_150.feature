Feature: Scrubber General Logic_150  When LCD L33628 applies to the claim, user enters 67228 and POS is NOT 11, 21, 22, 23, 24, 49.

  Scenario: Logic 150 Case1 When LCD L33628 applies to the claim, user enters 67228 and POS is NOT 11, 21, 22, 23, 24, 49.
    Given Logic 150 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic150 case1
	Then I should validate following for Logic150 case1
		| row | code | severity | message |
		| 1 | 19 | CRITICAL | Confirm POS. NGS does not list POS 19 as a payable POS for CPT 67228 (LCD L33628).|
		
  Scenario: Logic 150 Case2 When Billing provider speciality is not 65.
    Given Logic 150 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic150 case2
	Then I should validate following for Logic150 case2
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | Confirm POS. NGS does not list POS 11 as a payable POS for CPT 67228 (LCD L33628).|