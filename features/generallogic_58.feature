Feature: Scrubber General Logic_58 When a code from C0000-C9999 is entered. 

  Scenario: Logic 58 Case1 When a code from C0000-C9999 is entered. 
    Given Logic 58 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic58 case1
	Then I should validate following for Logic58 case1
		| row | code | severity | message |
		| 1 | C1715 | MEDIUM | The physician should not report this code. HCPCS C-codes are used by OPPS hospitals to report drugs, biological, and devices. C-codes can be reported for facility (technical) services only.|
		
  Scenario: Logic 58 Case2 When a code from C0000-C9999 is entered.
	Given Logic 58 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic58 case2
	Then I should validate following for Logic58 case2
		| row | code | severity | message |
		| 1 | C1718| MEDIUM | The physician should not report this code. HCPCS C-codes are used by OPPS hospitals to report drugs, biological, and devices. C-codes can be reported for facility (technical) services only.|