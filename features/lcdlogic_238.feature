Feature: Scrubber General Logic_238 when L33927 applies to the claim and User enters CPT 92136  And Primary Dx = Z96.1.

  Scenario: Logic 238 Case1 When CPT 92136  And Primary Dx = Z96.1.
    Given Logic 238 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic238 case1
	Then I should validate following for Logic238 case1
		| row | code | severity | message |
		| 1 | Z96.1 | CRITICAL | Do not report Z96.1 as the primary diagnosis (per Coding Guidelines attached with LCD L33927).|