Feature: Scrubber General Logic_114 when CPT 92225 AND 92226 and BOTH codes have modifier RT or LT or 1 CPT have modifier 50 and other have RT or LT.

  Scenario: Logic 114 Case1 When both codes have RT.
    Given Logic 114 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic114 case1
	Then I should validate following for Logic114 case1
		| row | code | severity | message |
		| 1 | 92226 | CRITICAL | NGS will not reimburse 92225 and 92226 on the same day for the same eye by the same provider. (NGS Article A52861).|
		
  Scenario: Logic 114 Case3 When one of the two codes (92225 and 92226) have RT and other have LT
    Given Logic 114 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic114 case3
	Then I should validate following for Logic114 case3
		| row | code | severity | message |
		| 1 | 92226 | CRITICAL | NGS will not reimburse 92225 and 92226 on the same day for the same eye by the same provider. (NGS Article A52861).|