Feature: Scrubber General Logic_6 Diagnosis code does not match or support the procedure. 
  
  Scenario: Logic 6 Case1 Diagnosis code does not match or support the procedure.
	Given Logic 6 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic6 case1
	Then I should validate following for logic6 case1
		| row | code | severity | message |
		| 1 | H57.10 | LOW | Diagnosis code may not match/support the procedure|
		
  Scenario: Logic 6 Case2 Diagnosis code match or support the procedure.
	Given Logic 6 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic6 case2
	Then I should validate following for logic6 case2
		| row | code | severity | message |
		| 1 | H20.21 | LOW | Diagnosis code may not match/support the procedure|
		
  Scenario: Logic 6 Case3 Secondary Diagnosis code not match or support the procedure.
	Given Logic 6 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic6 case3
	Then I should validate following for logic6 case3
		| row | code | severity | message |
		| 1 | H57.10 | LOW | Diagnosis code may not match/support the procedure|
		
  Scenario: Logic 6 Case4 Tertiary Diagnosis code not match or support the procedure.
	Given Logic 6 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic6 case4
	Then I should validate following for logic6 case4
		| row | code | severity | message |
		| 1 | H57.10 | LOW | Diagnosis code may not match/support the procedure|

  Scenario: Logic 6 Case5 Diagnosis code not match or support the enum procedure code.
	Given Logic 6 Case5 valid EDI
	When I hit the API with username and password with valid EDI for logic6 case5
	Then I should validate following for logic6 case5
		| row | code | severity | message |
		| 1 | H57.10 | LOW | Diagnosis code may not match/support the procedure|
		
  Scenario: Logic 6 Case6 Diagnosis code not match or support a procedure code from given two procedure codes.
	Given Logic 6 Case6 valid EDI
	When I hit the API with username and password with valid EDI for logic6 case6
	Then I should validate following for logic6 case6
		| row | code | severity | message |
		| 1 | H57.10 | LOW | Diagnosis code may not match/support the procedure|
		
  Scenario: Logic 6 Case7 Diagnosis code match or support both the given procedure code.
	Given Logic 6 Case7 valid EDI
	When I hit the API with username and password with valid EDI for logic6 case7
	Then I should validate following for logic6 case7
		| row | code | severity | message |
		| 1 | H57.10 | LOW | Diagnosis code may not match/support the procedure|
		
