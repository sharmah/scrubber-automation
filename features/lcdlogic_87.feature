Feature: Scrubber General Logic_87 When Claim contains LCD L33458, CPT from group 1-10 and does not contain hcpc - J0585, J0586, J0587, J0588

  Scenario: Logic 87 Case1 When only one claim line for CPT is entered.
    Given Logic 87 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic87 case1
	Then I should validate following for Logic87 case1
		| row | code | severity | message |
		| 1 | 46505 | MEDIUM | Did you supply separately reportable botulinumtoxin? LCD L33458 allows reporting of '.46505.' with 'J0585','J0586','J0587','J0588'.|
		
  Scenario: Logic 87 Case2 When CPT 64611 is entered with HCPCS J0587 is used with LCD L33458.
    Given Logic 87 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic87 case2
	Then I should validate following for Logic87 case2
		| row | code | severity | message |
		| 1 | 64611 | MEDIUM | Did you supply separately reportable botulinumtoxin? LCD L33458 allows reporting of '.64611.' with 'J0585','J0586','J0587','J0588'.|
		
  Scenario: Logic 87 Case3 When CPT 64611 is entered with HCPCS J0583 is used with LCD L33458.
    Given Logic 87 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic87 case3
	Then I should validate following for Logic87 case3
		| row | code | severity | message |
		| 1 | 64611 | MEDIUM | Did you supply separately reportable botulinumtoxin? LCD L33458 allows reporting of '.64611.' with 'J0585','J0586','J0587','J0588'.|
		
  Scenario: Logic 87 Case4 When CPT 64611 is entered with HCPCS J0586 is used with LCD L33458.
    Given Logic 87 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic87 case4
	Then I should validate following for Logic87 case4
		| row | code | severity | message |
		| 1 | 64611 | MEDIUM | Did you supply separately reportable botulinumtoxin? LCD L33458 allows reporting of '.64611.' with 'J0585','J0586','J0587','J0588'.|