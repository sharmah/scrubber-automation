Feature: Scrubber General Logic_152  When LCD L34760 applies to the claim and user enters <code 1>, which is either 92133-TC or 92134-TC  and POS <#> is NOT 11, 32, 49, 50, or 72

  Scenario: Logic 152 Case1 When POS entered is 19.
    Given Logic 152 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic152 case1
	Then I should validate following for Logic152 case1
		| row | code | severity | message |
		| 1 | 19 | CRITICAL | Confirm POS. WPS does not list POS 19 as a payable POS for 92133 (Billing and Coding Guidelines attached to LCD L34760).|
		
  Scenario: Logic 152 Case2 When POS entered is 72
    Given Logic 152 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic152 case2
	Then I should validate following for Logic152 case2
		| row | code | severity | message |
		| 1 | 72 | CRITICAL | Confirm POS. WPS does not list POS 72 as a payable POS for 92134 (Billing and Coding Guidelines attached to LCD L34760).|