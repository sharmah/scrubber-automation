Feature: Scrubber General Logic_137 When LCD L35094 applies to the claim and User enters 0378T.

  Scenario: Logic 137 Case1 When LCD L35094 applies to the claim and User enters 0378T.
    Given Logic 137 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic137 case1
	Then I should validate following for Logic137 case1
		| row | code | severity | message |
		| 1 | 11 | LOW | Novitas indicates 0378T is eligible for coverage only under clinical trials and/or IDE studies (LCD L35094). Refer to Medicare Claims Processing Manual, Chapter 32, Sections 68-69 for information on reporting these services (LCD L35094).|