Feature: Scrubber General Logic_5 Z-code unaccepted Primary Diagnosis code
  
  Scenario: Logic 5 Case1 Primary diagnosis code is z-code
	Given Logic 5 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic5 case1
	Then I should validate following for logic5 case1
		| row | code | severity | message |
		| 1 | Z96.1 | LOW | Z Code is an unacceptable principal diagnosis|
		
   Scenario: Logic 5 Case2 Secondary diagnosis code is z-code
	Given Logic 5 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic5 case2
	Then I should validate following for logic5 case2
		| row | code | severity | message |
		| 1 | Z96.1 | LOW | Z Code is an unacceptable principal diagnosis|

  Scenario: Logic 5 Case3 Primary diagnosis code is B-code
	Given Logic 5 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic5 case3
	Then I should validate following for logic5 case3
		| row | code | severity | message |
		| 1 | B60.13 | LOW | Z Code is an unacceptable principal diagnosis|
		
  Scenario: Logic 5 Case4 Primary diagnosis code is z-code 'Not in scrubber_zcode'
	Given Logic 5 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic5 case4
	Then I should validate following for logic5 case4
		| row | code | severity | message |
		| 1 | Z01.810 | LOW | Z Code is an unacceptable principal diagnosis|

  Scenario: Logic 5 Case5 Secondary diagnosis code is z-code 'Not in scrubber_zcode'
	Given Logic 5 Case5 valid EDI
	When I hit the API with username and password with valid EDI for logic5 case5
	Then I should validate following for logic5 case5
		| row | code | severity | message |
		| 1 | Z01.810 | LOW | Z Code is an unacceptable principal diagnosis|