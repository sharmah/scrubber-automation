Feature: Scrubber General Logic_59 WHENEVER G0463 is used.

  Scenario: Logic 59 Case1 WHENEVER G0463 is alone used in the claim.
    Given Logic 59 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic59 case1
	Then I should validate following for Logic59 case1
		| row | code | severity | message |
		| 1 | G0463 | CRITICAL | G0463 is for hospital use only, representing a clinic visit under OPPS. Provider should report appropriate code from 99201-99215 instead.|
		
  Scenario: Logic 59 Case2 G0463 is used at LX2 with other surgery code at LX1
	Given Logic 59 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic59 case2
	Then I should validate following for Logic59 case2
		| row | code | severity | message |
		| 1 | G0463| CRITICAL | G0463 is for hospital use only, representing a clinic visit under OPPS. Provider should report appropriate code from 99201-99215 instead.|
		
Scenario: Logic 59 Case3 When same code for same DOS is entered multiple times without a required modifier.
	Given Logic 59 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic59 case3
	Then I should validate following for Logic59 case3
		| row | code | severity | message |
		| 1 | G0463| CRITICAL | G0463 is for hospital use only, representing a clinic visit under OPPS. Provider should report appropriate code from 99201-99215 instead.|