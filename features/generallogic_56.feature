Feature: Scrubber General Logic_56 When same code for same DOS is entered multiple times without a required modifier.

  Scenario: Logic 56 Case1 When same code for same DOS is entered multiple times without a required modifier.
    Given Logic 56 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case1
	Then I should validate following for Logic56 case1
		| row | code | severity | message |
		| 1 | 71010 | MEDIUM | Duplicate Code: When same code applies multiple times on single DOS, report multiple units or append appropriate modifier (e.g., RT, LT, 76, 77).|
		
  Scenario: Logic 56 Case2 When same code for same DOS is entered twice with a required modifier on LX1.
	Given Logic 56 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case2
	Then I should validate following for Logic56 case2
		| row | code | severity | message |
		| 1 | 71010| MEDIUM | Duplicate Code: When same code applies multiple times on single DOS, report multiple units or append appropriate modifier (e.g., RT, LT, 76, 77).|
		
  Scenario: Logic 56 Case3 When same code for same DOS is entered twice with a required modifier on LX1.
	Given Logic 56 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case3
	Then I should validate following for Logic56 case3
		| row | code | severity | message |
		| 1 | 71010| MEDIUM | Duplicate Code: When same code applies multiple times on single DOS, report multiple units or append appropriate modifier (e.g., RT, LT, 76, 77).|
		
  Scenario: Logic 56 Case4 When same code for same DOS is entered twice with a required modifier on LX1.
	Given Logic 56 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case4
	Then I should validate following for Logic56 case4
		| row | code | severity | message |
		| 1 | 24655| MEDIUM | Duplicate Code: When same code applies multiple times on single DOS, report multiple units or append appropriate modifier (e.g., RT, LT, 76, 77).|
		
  Scenario: Logic 56 Case5 When same code for same DOS is entered twice with a required modifier on LX1.
	Given Logic 56 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case5
	Then I should validate following for Logic56 case5
		| row | code | severity | message |
		| 1 | 12056| MEDIUM | Duplicate Code: When same code applies multiple times on single DOS, report multiple units or append appropriate modifier (e.g., RT, LT, 76, 77).|
		
  Scenario: Logic 56 Case6 When same code for same DOS is entered twice with a required modifier on LX1.
	Given Logic 56 Case6 valid EDI
	When I hit the API with username and password with valid EDI for Logic56 case6
	Then I should validate following for Logic56 case6
		| row | code | severity | message |
		| 1 | 71010| MEDIUM | Duplicate Code: When same code applies multiple times on single DOS, report multiple units or append appropriate modifier (e.g., RT, LT, 76, 77).|
	