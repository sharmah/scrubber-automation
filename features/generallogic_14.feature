Feature: Scrubber General Logic_14 CCI edit col1 and col2 codes are bundled user may override the edit with a modifier.
  
  Scenario: Logic 14 Case1 CCI edit col1 and col2 codes are bundled, override the edit with a modifier. DOS is prior Jan, 2016
	Given Logic 14 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic14 case1
	Then I should validate following for logic14 case1
		| row | code | severity | message |
		| 2 | 10022 | MEDIUM | CCI edit: 10022 is bundled into 40490 You may override the edit with a modifier (58, 59, 78, 79, 91, LT, RT, E1, E2, E3, E4, FA, F1, F2, F3, F4, F5, F6, F7, F8, F9, TA, T1, T2, T3, T4, T5, T6, T7, T8, T9, LC, LD, RC, LM, RI, XE, XP, XS, XU) on 10022 if circumstances and documentation support 10022 being distinct from 40490|
		
  Scenario: Logic 14 Case2 CCI edit col1 and col2 codes are bundled, override the edit with a modifier. Mod XP appended.
	Given Logic 14 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic14 case2
	Then I should validate following for logic14 case2
		| row | code | severity | message |
		| 1 | 10022 | MEDIUM | CCI edit: 10022 is bundled into 40490 You may override the edit with a modifier (58, 59, 78, 79, 91, LT, RT, E1, E2, E3, E4, FA, F1, F2, F3, F4, F5, F6, F7, F8, F9, TA, T1, T2, T3, T4, T5, T6, T7, T8, T9, LC, LD, RC, LM, RI, XE, XP, XS, XU) on 10022 if circumstances and documentation support 10022 being distinct from 40490|
		
  Scenario: Logic 14 Case3 CCI edit col1 and col2 codes are bundled, override the edit with a modifier. DOS is prior Jan, 2016
	Given Logic 14 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic14 case3
	Then I should validate following for logic14 case3
		| row | code | severity | message |
		| 1 | 61650 | MEDIUM | CCI edit: 61650 is bundled into 40490 You may override the edit with a modifier (58, 59, 78, 79, 91, LT, RT, E1, E2, E3, E4, FA, F1, F2, F3, F4, F5, F6, F7, F8, F9, TA, T1, T2, T3, T4, T5, T6, T7, T8, T9, LC, LD, RC, LM, RI, XE, XP, XS, XU) on 61650 if circumstances and documentation support 61650 being distinct from 40490|
		
