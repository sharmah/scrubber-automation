Feature: Scrubber General Logic_51 when claim have two CPT code and one one the CPT code is of 'T' Status.

  Scenario: Logic 51 Case1 When no 'T' status code are there.
    Given Logic 51 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic51 case1
	Then I should validate following for Logic51 case1
		| row | code | severity | message |
		| 1 | G0117 | MEDIUM | Not reportable per Medicare Physician Fee Schedule T status: Code is bundled into any same day service|
		
  Scenario: Logic 51 Case2 When 'T' status code 36592 is entered with 88304
	Given Logic 51 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic51 case2
	Then I should validate following for Logic51 case2
		| row | code | severity | message |
		| 1 | 36592 | MEDIUM | Not reportable per Medicare Physician Fee Schedule T status: Code is bundled into any same day service|
		
  Scenario: Logic 51 Case3 When two 'T' codes are entered.
	Given Logic 51 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic51 case3
	Then I should validate following for Logic51 case3
		| row | code | severity | message |
		| 1 | 36591 | MEDIUM | Not reportable per Medicare Physician Fee Schedule T status: Code is bundled into any same day service|
		
Scenario: Logic 51 Case4 When 'T' status code G0118 is entered with 0378T
	Given Logic 51 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic51 case4
	Then I should validate following for Logic51 case4
		| row | code | severity | message |
		| 1 | G0118 | MEDIUM | Not reportable per Medicare Physician Fee Schedule T status: Code is bundled into any same day service|
