Feature: Scrubber General Logic_241 When L33927 applies to the claim And CPT 92136 is entered with Mod. 50 and Without Mod 26 or TC.

  Scenario: Logic 241 Case1 When CPT 92136 is entered with Mod. 50 and Without Mod 26 or TC.
    Given Logic 241 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic241 case1
	Then I should validate following for Logic241 case1
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Don’t use modifier 50 to indicate services performed bilaterally on the same date of service. Global 92136 is a bilateral service, and reimbursement is based on the total service for both eyes (per Coding Guidelines attached with LCD L33927).|