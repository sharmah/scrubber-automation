Feature: Scrubber General Logic_234 When L33810 applies to claim and CPT <code1> is 92025 and PDx is not equal to H52.211-H52.219, H52.221-H52.229.

  Scenario: Logic 234 Case1 When CPT <code1> is 92025 and PDx is equal to H52.211.
    Given Logic 234 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic234 case1
	Then I should validate following for Logic234 case1
		| row | code | severity | message |
		| 1 | 92025 | CRITICAL | Missing Primary Diagnosis (PDx) code Per LCD (Policy # L33810) coverage for this state and contractor|
		
  Scenario: Logic 234 Case2 When CPT <code1> is 92025 and PDx is H53.2 & SDx is equal to H52.211.
    Given Logic 234 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic234 case2
	Then I should validate following for Logic234 case2
		| row | code | severity | message |
		| 1 | 92025 | CRITICAL | Missing Primary Diagnosis (PDx) code Per LCD (Policy # L33810) coverage for this state and contractor|
		
  Scenario: Logic 234 Case3 When CPT <code1> is 92025 and PDx is not equal to H52.211-H52.219, H52.221-H52.229. 
    Given Logic 234 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic234 case3
	Then I should validate following for Logic234 case3
		| row | code | severity | message |
		| 1 | 92025 | CRITICAL | Missing Primary Diagnosis (PDx) code Per LCD (Policy # L33810) coverage for this state and contractor|