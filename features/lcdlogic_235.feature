Feature: Scrubber General Logic_235 When L33810 applies to claim and CPT <code1> is 92025 and PDx is equal to H52.211-H52.219, H52.221-H52.229 and Secondary Dx is not equal to Z98.41-Z98.49 or Z98.83.

  Scenario: Logic 235 Case1 When CPT <code1> is 92025 and PDx is equal to H52.211-H52.219, H52.221-H52.229 and Secondary Dx is not entered.
    Given Logic 235 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic235 case1
	Then I should validate following for Logic235 case1
		| row | code | severity | message |
		| 1 | 92025 | CRITICAL | Missing Secondary Diagnosis (Z98.41,Z98.42,Z98.49,Z98.83) code Per LCD (Policy # L33810) coverage for this state and contractor|
		
  Scenario: Logic 235 Case2 When CPT <code1> is 92025 and PDx is equal to H52.211-H52.219, H52.221-H52.229 and Secondary Dx is not equal to Z98.41-Z98.49 or Z98.83.
    Given Logic 235 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic235 case2
	Then I should validate following for Logic235 case2
		| row | code | severity | message |
		| 1 | Z94.7 | CRITICAL | Missing Secondary Diagnosis (Z98.41,Z98.42,Z98.49,Z98.83) code Per LCD (Policy # L33810) coverage for this state and contractor|
		
  Scenario: Logic 235 Case3 When CPT <code1> is 92025 and PDx is equal to H52.211-H52.219, H52.221-H52.229 and Secondary Dx is equal to Z98.41-Z98.49 or Z98.83.
    Given Logic 235 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic235 case3
	Then I should validate following for Logic235 case3
		| row | code | severity | message |
		| 1 | Z98.41 | CRITICAL | Missing Secondary Diagnosis (Z98.41,Z98.42,Z98.49,Z98.83) code Per LCD (Policy # L33810) coverage for this state and contractor|