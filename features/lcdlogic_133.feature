Feature: Scrubber General Logic_133  LCD L34615 applies to the claim and code 1 is either 92081-TC, 92082-TC, or 92083-TC AND POS <#> is NOT 11, 32, 49, 50, or 72.

  Scenario: Logic 133 Case1 When code 92081-TC and POS 20 is entered.
    Given Logic 133 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic133 case1
	Then I should validate following for Logic133 case1
		| row | code | severity | message |
		| 1 | 20 | CRITICAL | Confirm POS. WPS does not list POS 20 as a payable POS for 92081 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 133 Case2 When code 92081-TC and POS 32 is entered.
    Given Logic 133 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic133 case2
	Then I should validate following for Logic133 case2
		| row | code | severity | message |
		| 1 | 32 | CRITICAL | Confirm POS. WPS does not list POS 32 as a payable POS for 92081 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 133 Case3 When code 92081-26 and POS 50 is entered.
    Given Logic 133 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic133 case3
	Then I should validate following for Logic133 case3
		| row | code | severity | message |
		| 1 | 50 | CRITICAL | Confirm POS. WPS does not list POS 50 as a payable POS for 92081 (LCD L34615, Billing and Coding Guidelines attachment).|