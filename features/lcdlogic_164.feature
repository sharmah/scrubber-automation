Feature: Scrubber General Logic_164 When LCD L34171 applies to the claim having CPT <code> 68801, 68810 to 68815, or 68840 and <code>  have both the modifier RT and LT appended.

  Scenario: Logic 164 Case1 When LCD L34171 applies to the claim having CPT <code> 68801, 68810 to 68815, or 68840 and <code>  have both the modifier RT and LT appended.
    Given Logic 164 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic164 case1
	Then I should validate following for Logic164 case1
		| row | code | severity | message |
		| 1 | 68810 | CRITICAL | For a bilateral service, append 50 to 68810, not LT and RT (CGS Article A52391).|