Feature: Scrubber General Logic_66 When claim have 2 or more codes with no CCI bundling and the specified Mod is not been assigned.

  Scenario: Logic 66 Case1 Two Code from range 12001 – 13160 is entered have CCI bundling without Mod 59/XE/XP/XS/XU.
    Given Logic 66 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic66 case1
	Then I should validate following for Logic66 case1
		| row | code | severity | message |
		| 1 | 13120 | LOW | When more than one classification of wounds is repaired, you may list the more complicated as the primary procedure and the less complicated as the secondary procedure, using modifier 59, provided CCI edits do not bundle the codes together. |
		
  Scenario: Logic 66 Case2 Two Code from range 12001 – 13160 is entered have no CCI bundling without Mod 59/XE/XP/XS/XU.
	Given Logic 66 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic66 case2
	Then I should validate following for Logic66 case2
		| row | code | severity | message |
		| 1 | 13120 | LOW | When more than one classification of wounds is repaired, you may list the more complicated as the primary procedure and the less complicated as the secondary procedure, using modifier 59, provided CCI edits do not bundle the codes together.  |
		
  Scenario: Logic 66 Case3 Two Code from range 12001 – 13160 is entered have no CCI bundling without Mod 59/XE/XP/XS/XU.
    Given Logic 66 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic66 case3
	Then I should validate following for Logic66 case3
		| row | code | severity | message |
		| 1 | 12001 | LOW | When more than one classification of wounds is repaired, you may list the more complicated as the primary procedure and the less complicated as the secondary procedure, using modifier 59, provided CCI edits do not bundle the codes together.  |
		
  Scenario: Logic 66 Case4 Two Code from range 12001 – 13160 is entered have no CCI bundling with Mod 59/XE/XP/XS/XU on code2
    Given Logic 66 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic66 case4
	Then I should validate following for Logic66 case4
		| row | code | severity | message |
		| 1 | 12001 | LOW | When more than one classification of wounds is repaired, you may list the more complicated as the primary procedure and the less complicated as the secondary procedure, using modifier 59, provided CCI edits do not bundle the codes together. |
		
  Scenario: Logic 66 Case5 Two Code from range 12001 – 13160 is entered have no CCI bundling with Mod 59/XE/XP/XS/XU on code1
	Given Logic 66 Case5 valid EDI 
	When I hit the API with username and password with valid EDI for Logic66 case5
	Then I should validate following for Logic66 case5
		| row | code | severity | message |
		| 1 | 12031 | LOW | When more than one classification of wounds is repaired, you may list the more complicated as the primary procedure and the less complicated as the secondary procedure, using modifier 59, provided CCI edits do not bundle the codes together. |
