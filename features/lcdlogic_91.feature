Feature: Scrubber General Logic_91 When ICD-10 code (not group1 and group3) linked to CPT code 92227 and L33467 applies to the claim.

  Scenario: Logic 91 Case1 When D3131 (grp 1) linked to CPT code 92227 and L33467 applies to the claim.
    Given Logic 91 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic91 case1
	Then I should validate following for Logic91 case1
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | Confirm ICD-10 code choice. Palmetto does not list D31.31 as a code that supports medical necessity for 92227 (LCD L33467). Note that the LCD indicates 92227 “is not for routine screening, but is covered for evaluation of asymptomatic patients at risk with known disease (e.g. diabetes mellitus) that is likely to cause retinal disease when the test is ordered by the treating physician.|
		
  Scenario: Logic 91 Case2 When H05.332 (not in grp 1, grp 3) linked to CPT code 92227 and L33467 applies to the claim.
    Given Logic 91 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic91 case2
	Then I should validate following for Logic91 case2
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | Confirm ICD-10 code choice. Palmetto does not list H05.332 as a code that supports medical necessity for 92227 (LCD L33467). Note that the LCD indicates 92227 “is not for routine screening, but is covered for evaluation of asymptomatic patients at risk with known disease (e.g. diabetes mellitus) that is likely to cause retinal disease when the test is ordered by the treating physician.|
		
  Scenario: Logic 91 Case3 When PDx B20 (grp3) and SDx H21.41(not in grp 1, grp 3) linked to CPT code 92227 and L33467 applies to the claim.
    Given Logic 91 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic91 case3
	Then I should validate following for Logic91 case3
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | Confirm ICD-10 code choice. Palmetto does not list H21.41 as a code that supports medical necessity for 92227 (LCD L33467). Note that the LCD indicates 92227 “is not for routine screening, but is covered for evaluation of asymptomatic patients at risk with known disease (e.g. diabetes mellitus) that is likely to cause retinal disease when the test is ordered by the treating physician.|