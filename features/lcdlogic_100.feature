Feature: Scrubber General Logic_100 When Group1 CPT is entered to the claim on which LCD L33558 is applied and not appended modifier LT and/or RT to the Group 1 CPT code.

  Scenario: Logic 100 Case1 When Mod RT is appended
    Given Logic 100 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic100 case1
	Then I should validate following for Logic100 case1
		| row | code | severity | message |
		| 1 | 66940 | CRITICAL | Append modifier LT or RT to 66940 (NGS Article A52819)|
		
  Scenario: Logic 100 Case2 When Mod 50 is appended
    Given Logic 100 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic100 case2
	Then I should validate following for Logic100 case2
		| row | code | severity | message |
		| 1 | 66940 | CRITICAL | Append modifier LT or RT to 66940 (NGS Article A52819)|
		
  Scenario: Logic 100 Case3 When no RT/LT or 50 appended
    Given Logic 100 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic100 case3
	Then I should validate following for Logic100 case3
		| row | code | severity | message |
		| 1 | 66940 | CRITICAL | Append modifier LT or RT to 66940 (NGS Article A52819)|
		
  Scenario: Logic 100 Case4 When article A52819 gets expire. It is getting expired on 1st May 2016
    Given Logic 100 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic100 case4
	Then I should validate following for Logic100 case4
		| row | code | severity | message |
		| 1 | 66940 | CRITICAL | Append modifier LT or RT to 66940 (NGS Article A52819)|
		
  Scenario: Logic 100 Case5 When both RT and LT appended.
    Given Logic 100 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic100 case5
	Then I should validate following for Logic100 case5
		| row | code | severity | message |
		| 1 | 66940 | CRITICAL | Append modifier LT or RT to 66940 (NGS Article A52819)|