Feature: Scrubber General Logic_166 when LCD L34171 applies to the claim having CPT <code> 68801 with relevant modifier in db or on same claim and user enters <code1> 68811 or 68815 for same DOS and modifier.

  Scenario: Logic 166 Case1 When CPT 68801-RT on same claim and user enters 68811 or 68815 for same DOS and modifier RT.
    Given Logic 166 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic166 case1
	Then I should validate following for Logic166 case1
		| row | code | severity | message |
		| 1 | 68801 | CRITICAL | CGS will pay for only 68801 when reported on the same DOS as 68801 (CGS Article A52391).|
		
  Scenario: Logic 166 Case2 When CPT 68801-LT on same claim and user enters 68811 or 68815 for same DOS and modifier LT.
    Given Logic 166 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic166 case2
	Then I should validate following for Logic166 case2
		| row | code | severity | message |
		| 1 | 68801 | CRITICAL | CGS will pay for only 68801 when reported on the same DOS as 68801 (CGS Article A52391).|