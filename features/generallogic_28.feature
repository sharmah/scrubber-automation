Feature: Scrubber General Logic_28 Modifier 25 is associated to a CPT code other than E/M code.
  
  Scenario: Logic 28 Case1 Modifier 25 is associated to a CPT code other than E/M code.
	Given Logic 28 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic28 case1
	Then I should validate following for Logic28 case1
		| row | code | severity | message |
		| 1 | 69210 | CRITICAL | Invalid modifier: Modifier 25 can be used only with E/M procedure codes|
		
  Scenario: Logic 28 Case2 Modifier 25 is associated to an E/M code.
	Given Logic 28 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic28 case2
	Then I should validate following for Logic28 case2
		| row | code | severity | message |
		| 1 | 99203 | CRITICAL | Invalid modifier: Modifier 25 can be used only with E/M procedure codes|