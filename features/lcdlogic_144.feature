Feature: Scrubber General Logic_144 when L34413 applies to the claim and user enters CPT Code from group1 and And <ICD-10 code> exist from Group 1 codes which supports medical necessity. 

  Scenario: Logic 144 Case1 when  L34413 applies to the claim and user enters CPT Code from group1 and And <ICD-10 code> exist from Group 1 codes which supports medical necessity. 
    Given Logic 144 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic144 case1
	Then I should validate following for Logic144 case1
		| row | code | severity | message |
		| 1 | 66830 | LOW | Check for the Documentation with 66830 for Visually-Symptomatic cataract. There is requirement of documentation for Visually Symptomatic cataract, per LCD L34413.|