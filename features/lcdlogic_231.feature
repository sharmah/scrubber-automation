Feature: Scrubber General Logic_231 When L34008 applies to the claim and code 92025 is entered with code1 65771, 65710-65756, 65770 on the same DOS as CPT code 92025.

  Scenario: Logic 231 Case1 When code 92025 is entered with code1 65771, 65710-65756, 65770 on the same DOS as CPT code 92025.
    Given Logic 231 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic231 case1
	Then I should validate following for Logic231 case1
		| row | code | severity | message |
		| 1 | 92025 | CRITICAl | CGS will not cover 92025 if performed pre- or post-operatively in relation to a procedure Medicare does not cover, such as 65710 (LCD L34008).|