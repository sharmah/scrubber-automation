Feature: Scrubber General Logic_242 When L33927 applies to the claim and CPT 92136 is appended with Mod TC and 50.

  Scenario: Logic 242 Case1 When CPT 92136 is appended with Mod TC and 50.
    Given Logic 242 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic242 case1
	Then I should validate following for Logic242 case1
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Don’t use modifier 50 to indicate services performed bilaterally on the same date of service. 92136-TC is a bilateral service, and reimbursement is based on the total service for both eyes (per Coding Guidelines attached with LCD L33927).|