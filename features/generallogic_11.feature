Feature: Scrubber General Logic_11 Procedure and sex conflict
  
  Scenario: Logic 11 Case1 Procedure and sex conflict
	Given Logic 11 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic11 case1
	Then I should validate following for logic11 case1
		| row | code | severity | message |
		| 1 | 58262 | CRITICAL | Procedure and sex conflict|
		
  Scenario: Logic 11 Case2 Procedure and sex conflict - Correct Gender and procedure match
	Given Logic 11 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic11 case2
	Then I should validate following for logic11 case2
		| row | code | severity | message |
		| 1 | 58262 | CRITICAL | Procedure and sex conflict|
		
  Scenario: Logic 11 Case3 Procedure and sex conflict - Correct Gender and procedure match
	Given Logic 11 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic11 case3
	Then I should validate following for logic11 case3
		| row | code | severity | message |
		| 1 | 99213 | CRITICAL | Procedure and sex conflict|
		
  Scenario: Logic 11 Case4 Procedure and sex conflict - Two Px entered in the claim
	Given Logic 11 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic11 case4
	Then I should validate following for logic11 case4
		| row | code | severity | message |
		| 1 | 52647 | CRITICAL | Procedure and sex conflict|
		
  Scenario: Logic 11 Case5 Procedure and sex conflict - No gender entered
	Given Logic 11 Case5 valid EDI
	When I hit the API with username and password with valid EDI for logic11 case5
	Then I should validate following for logic11 case5
		| row | code | severity | message |
		| 1 | 58262 | CRITICAL | Procedure and sex conflict|
		
  Scenario: Logic 11 Case6 Procedure and sex conflict - 'U' entered as gender
	Given Logic 11 Case6 valid EDI
	When I hit the API with username and password with valid EDI for logic11 case6
	Then I should validate following for logic11 case6
		| row | code | severity | message |
		| 1 | 58262 | CRITICAL | Procedure and sex conflict|
		
