Feature: Scrubber General Logic_40 Mod 53 is used with the Px
  
  Scenario: Logic 40 Case1 Mod 53 is used with the Px
	Given Logic 40 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic40 case1
	Then I should validate following for Logic40 case1
		| row | code | severity | message |
		| 1 | 92081 | LOW | Modifier 53 is not appropriate for use by an ASC/outpatient hospital (see modifiers 73 and 74 instead).|
		
  Scenario: Logic 40 Case2 Mod 53 is used with the Px
	Given Logic 40 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic40 case2
	Then I should validate following for Logic40 case2
		| row | code | severity | message |
		| 1 | 77053 | LOW | Modifier 53 is not appropriate for use by an ASC/outpatient hospital (see modifiers 73 and 74 instead).|
		
  Scenario: Logic 40 Case3 Mod 53 is used with the Px
	Given Logic 40 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic40 case3
	Then I should validate following for Logic40 case3
		| row | code | severity | message |
		| 1 | 66170 | LOW | Modifier 53 is not appropriate for use by an ASC/outpatient hospital (see modifiers 73 and 74 instead).|
		
  Scenario: Logic 40 Case4 Mod 53 is used with the Px
	Given Logic 40 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic40 case4
	Then I should validate following for Logic40 case4
		| row | code | severity | message |
		| 1 | 92081 | LOW | Modifier 53 is not appropriate for use by an ASC/outpatient hospital (see modifiers 73 and 74 instead).|