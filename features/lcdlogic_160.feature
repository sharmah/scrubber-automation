Feature: Scrubber General Logic_160 When LCD L33574 applies to the claim and <code>, which is 92081, 92082, or 92083 entered with modifier TC and POS is NOT  11, 32, 49, 50, or 72 and DOS for <code> is on or between Jan. 1, 2016, and May 1, 2016.

  Scenario: Logic 160 Case1 When CPT 92083-TC is entered, POS is 19 and DOS is between Jan. 1, 2016, and May 1, 2016.
    Given Logic 160 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic160 case1
	Then I should validate following for Logic160 case1
		| row | code | severity | message |
		| 1 | 19 | CRITICAL | Confirm POS. NGS does not list POS 19 as a payable POS for 92083 (NGS Article A52829).|
		
  Scenario: Logic 160 Case2 When CPT 92083-TC is entered, POS is 19 and DOS is after May 1, 2016.
    Given Logic 160 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic160 case2
	Then I should validate following for Logic160 case2
		| row | code | severity | message |
		| 1 | 19 | CRITICAL | Confirm POS. NGS does not list POS 19 as a payable POS for 92083 (NGS Article A52829).|
		
  Scenario: Logic 160 Case3 When CPT 92083-TC is entered, POS is 32 and DOS is between Jan. 1, 2016, and May 1, 2016.
    Given Logic 160 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic160 case3
	Then I should validate following for Logic160 case3
		| row | code | severity | message |
		| 1 | 32 | CRITICAL | Confirm POS. NGS does not list POS 19 as a payable POS for 92083 (NGS Article A52829).|