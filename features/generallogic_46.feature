Feature: Scrubber General Logic_46 When +99100 is entered with Anesthesia code 00100-01999 for age range from 1-70 .
  
  Scenario: Logic 46 Case1 Anesthesia code 00140 is entered without +99100 and age is 70 years
    Given Logic 46 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic46 case1
	Then I should validate following for Logic46 case1
		| row | code | severity | message |
		| 1 | 00140 | CRITICAL | +99100 cannot be reported with this anesthesia code due to age conflict|
		
  Scenario: Logic 46 Case2 Anesthesia code 00140 is entered without +99100 and age is 58 years.
	Given Logic 46 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic46 case2
	Then I should validate following for Logic46 case2
		| row | code | severity | message |
		| 1 | 00140 | CRITICAL | +99100 cannot be reported with this anesthesia code|
		
  Scenario: Logic 46 Case3 Code +99100 is entered with Anesthesia code 00140 and age is 70 years and 1 day.
	Given Logic 46 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic46 case3
	Then I should validate following for Logic46 case3
		| row | code | severity | message |
		| 1 | 00140 | CRITICAL | +99100 cannot be reported with this anesthesia code|