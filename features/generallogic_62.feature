Feature: Scrubber General Logic_62 When 'To' DOS prior 'From' DOS.

  Scenario: Logic 62 Case1 When 'To' DOS prior 'From' DOS.
    Given Logic 62 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic62 case1
	Then I should validate following for Logic62 case1
		| row | code | severity | message |
		| 1 | 99213 | CRITICAL | "To" DOS cannot be prior to "From" DOS for a service|
		
  Scenario: Logic 62 Case2 When 'To' DOS prior 'From' DOS.
	Given Logic 62 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic62 case2
	Then I should validate following for Logic62 case2
		| row | code | severity | message |
		| 1 | G0202| CRITICAL | "To" DOS cannot be prior to "From" DOS for a service|
