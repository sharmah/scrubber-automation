Feature: Scrubber General Logic_179 When LCD L34528 applies to the claim and user enters <CPT 15820 or 15821> and Modifier GY is not linked with CPT.

  Scenario: Logic 179 Case1 When CPT 15820 is entered with No MOD GY.
    Given Logic 179 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic179 case1
	Then I should validate following for Logic179 case1
		| row | code | severity | message |
		| 1 | 15820 | CRITICAL | Medicare generally considers lower eyelid blepharoplasty (15820) to be a cosmetic procedure and a non-covered service. Append modifier GY for a non-covered denial (Billing & Coding Guidelines with LCD L34528).|
		
  Scenario: Logic 179 Case2 When CPT 15821 is entered with MOD GY.
    Given Logic 179 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic179 case2
	Then I should validate following for Logic179 case2
		| row | code | severity | message |
		| 1 | 15821 | CRITICAL | Medicare generally considers lower eyelid blepharoplasty (15820) to be a cosmetic procedure and a non-covered service. Append modifier GY for a non-covered denial (Billing & Coding Guidelines with LCD L34528).|