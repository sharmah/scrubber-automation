Feature: Scrubber General Logic_7 Diagnosis code not lie under LCD for the state or contractor.
  
  Scenario: Logic 7 Case1 Diagnosis not lie under LCD for the state or contractor.
	Given Logic 7 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic7 case1
	Then I should validate following for logic7 case1
		| row | code | severity | message |
		| 1 | H57.10 | CRITICAL | Diagnosis code is not under LCD 35091 coverage for this state and contractor |
		
  Scenario: Logic 7 Case2 Diagnosis lie under LCD for the state or contractor.
	Given Logic 7 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic7 case2
	Then I should validate following for logic7 case2
		| row | code | severity | message |
		| 1 | H57.10 | CRITICAL | Diagnosis code is not under LCD 34287 coverage for this state and contractor |
		
  Scenario: Logic 7 Case3 Diagnosis lie under LCD for the state or contractor.
	Given Logic 7 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic7 case3
	Then I should validate following for logic7 case3
		| row | code | severity | message |
		| 1 | H53.2 | CRITICAL | Diagnosis code is not under LCD 34287 coverage for this state and contractor |
		
  Scenario: Logic 7 Case4 Diagnosis not lie under LCD for the state or contractor.
	Given Logic 7 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic7 case4
	Then I should validate following for logic7 case4
		| row | code | severity | message |
		| 1 | H53.2 | CRITICAL | Diagnosis code is not under LCD 34287 coverage for this state and contractor |