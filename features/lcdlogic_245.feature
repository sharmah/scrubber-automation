Feature: Scrubber General Logic_245 When LCD L34594 applies to the claim and CPT entered is CPT 95905 and Either Specialty code is 65 or Taxonomy codes is 225100000X, 2251C2600X, 2251E1300X, 2251E1200X, 2251G0304X, 2251H1200X, 2251H1300X, 2251N0400X ,2251X0800X, 2251P0200X, 2251S0007X. 

  Scenario: Logic 245 Case1 When CPT entered is CPT 95905 and Either Specialty code is 65.
    Given Logic 245 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic245 case1
	Then I should validate following for Logic245 case1
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | WPS doesn’t allow 95905 by physical therapists (LCD L34594).|
		
  Scenario: Logic 245 Case2 When CPT entered is CPT 95905 and Either Specialty code is other than 65.
    Given Logic 245 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic245 case2
	Then I should validate following for Logic245 case2
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | WPS doesn’t allow 95905 by physical therapists (LCD L34594).|