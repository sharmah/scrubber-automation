Feature: Scrubber General When screening mammography code (77052 or 77057, or G0202 ) is used for male patients.

  Scenario: Logic 60 Case1 When screening mammography code (77052 or 77057 ) is used for female patients.
    Given Logic 60 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic60 case1
	Then I should validate following for Logic60 case1
		| row | code | severity | message |
		| 1 | 77052 | CRITICAL | Procedure and sex conflict. Medicare does not cover screening mammography for male patients. Other payers may apply a similar rule.|
		
  Scenario: Logic 60 Case2 When screening mammography code G0202 is used for female patients.
	Given Logic 60 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic60 case2
	Then I should validate following for Logic60 case2
		| row | code | severity | message |
		| 1 | G0202| CRITICAL | Procedure and sex conflict. Medicare does not cover screening mammography for male patients. Other payers may apply a similar rule.|
		
  Scenario: Logic 60 Case3 When screening mammography code (77052 or 77057 ) is used for male patients.
	Given Logic 60 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic60 case3
	Then I should validate following for Logic60 case3
		| row | code | severity | message |
		| 1 | 77052| CRITICAL | Procedure and sex conflict. Medicare does not cover screening mammography for male patients. Other payers may apply a similar rule.|
		
  Scenario: Logic 60 Case4 When screening mammography code G0202 is used for female patient and icd10 is male specific.
	Given Logic 60 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic60 case4
	Then I should validate following for Logic60 case4
		| row | code | severity | message |
		| 1 | G0202| CRITICAL | Procedure and sex conflict. Medicare does not cover screening mammography for male patients. Other payers may apply a similar rule.|
		
  Scenario: Logic 60 Case5 When screening mammography code G0202 is used for transgender patient and icd10 is female specific.
	Given Logic 60 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic60 case5
	Then I should validate following for Logic60 case5
		| row | code | severity | message |
		| 1 | 77052| CRITICAL | Procedure and sex conflict. Medicare does not cover screening mammography for male patients. Other payers may apply a similar rule.|
		
  Scenario: Logic 60 Case6 When screening mammography code G0202 is used for transgender patient and icd10 is male specific.
	Given Logic 60 Case6 valid EDI
	When I hit the API with username and password with valid EDI for Logic60 case6
	Then I should validate following for Logic60 case6
		| row | code | severity | message |
		| 1 | G0202| CRITICAL | Procedure and sex conflict. Medicare does not cover screening mammography for male patients. Other payers may apply a similar rule.|