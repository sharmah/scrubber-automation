Feature: Scrubber General Logic_145 When LCD L34512 applies to the claim of CPT 76514 and ICD10 from group1 except few specific ICDs and similar claim exist in history with DOS prior than current DOS.

  Scenario: Logic 145 Case1 Previous claim
    Given Logic 145 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic145 case1
	Then I should validate following for Logic145 case1
		| row | code | severity | message |
		| 1 | 66830 | CRITICAL | Palmetto does not allow corneal pachymetry more than once in a lifetime per provider (Palmetto Article A34512.|
		
  Scenario: Logic 145 Case2 When L35008 applies to the claim and no ABN modifier is associated to the CPT (Group 1)
    Given Logic 145 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic145 case2
	Then I should validate following for Logic145 case2
		| row | code | severity | message |
		| 1 | 66830 | CRITICAL | Palmetto does not allow corneal pachymetry more than once in a lifetime per provider (Palmetto Article A34512.|