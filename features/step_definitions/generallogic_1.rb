$response = ""
#$FilePath = "D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files"

Given (/^Logic 1 Case1 valid EDI$/) do
	file = File.open("#{$edi_open}\\Logic1\\Logic1_Case1.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case1$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case1$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case2 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case2.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case2$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case2$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	#puts $response
	#puts "\n\r"
	edi_values_to_check.hashes.each_with_index do |(v),i|
	#puts v['message']
	#puts "\n\r"
	#puts scrubber_response['results'][0]['claim']['claimResults'][i]['message']
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']))
			if (v['severity'] != (scrubber_response['results'][0]['claim']['claimResults'][i]['severity'])&& v['message'] != (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
				case_truthness = true
			end
		else
			case_truthness = true
		end 
	end
end

Given (/^Logic 1 Case3 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case3.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case3$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case3$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case4 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case4.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case4$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case4$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case5 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case5.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case5$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case5$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']))
			if (v['severity'] != (scrubber_response['results'][0]['claim']['claimResults'][i]['severity'])&& v['message'] != (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
				case_truthness = true
			end
		else
			case_truthness = true
		end 
	end
end

Given (/^Logic 1 Case6 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case6.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case6$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case6$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case7 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case7.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case7$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case7$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case8 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case8.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case8$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case8$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case9 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case9.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case9$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case9$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case10 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case10.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case10$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case10$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']) && v['severity'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['severity']) && v['message'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
			case_truthness = true
		end
	end 
end

Given (/^Logic 1 Case11 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic1\\Logic1_Case11.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for logic1 case11$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for logic1 case11$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	edi_values_to_check.hashes.each_with_index do |(v),i|
		if (v['code'] == (scrubber_response['results'][0]['claim']['claimResults'][i]['code']))
			if (v['severity'] != (scrubber_response['results'][0]['claim']['claimResults'][i]['severity'])&& v['message'] != (scrubber_response['results'][0]['claim']['claimResults'][i]['message']))
				case_truthness = true
			end
		else
			case_truthness = true
		end 
	end
end
