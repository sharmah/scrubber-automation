Feature: Scrubber General Logic_132 When <code 1>92081, 92082, or 92083, LCD L34615 applies to the claim where POS is NOT 11, 32, or 49"

  Scenario: Logic 132 Case1 When <code 1> 92081 and POS 11 is entered.
    Given Logic 132 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic132 case1
	Then I should validate following for Logic132 case1
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | Confirm POS. WPS does not list POS 11 as a payable POS for 92081 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 132 Case2 When <code 1> 92081 and POS 20 is entered.
    Given Logic 132 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic132 case2
	Then I should validate following for Logic132 case2
		| row | code | severity | message |
		| 1 | 20 | CRITICAL | Confirm POS. WPS does not list POS 20 as a payable POS for 92081 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 132 Case3 When <code 1> 92083 and POS 11 is entered.
    Given Logic 132 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic132 case3
	Then I should validate following for Logic132 case3
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | Confirm POS. WPS does not list POS 11 as a payable POS for 92083 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 132 Case4 When <code 1> 92083 and POS 25 is entered.
    Given Logic 132 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic132 case4
	Then I should validate following for Logic132 case4
		| row | code | severity | message |
		| 1 | 25 | CRITICAL | Confirm POS. WPS does not list POS 25 as a payable POS for 92083 (LCD L34615, Billing and Coding Guidelines attachment).|