Feature: Scrubber General Logic_228 When L34017 applies to the claim and <code 1> 92225 or 92226 is entered, Mod used is 50

  Scenario: Logic 228 Case1 When CPT <code 1> 92225 or 92226 and Mod 50 is entered.
    Given Logic 228 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic228 case1
	Then I should validate following for Logic228 case1
		| row | code | severity | message |
		| 1 | 92225 | MEDIUM | Do not append modifier 50 to 92225. To report services on both eyes, report 92225 on two separate lines and append modifier LT to one and RT to the other (LCD L34017, coding guidelines attachment).|