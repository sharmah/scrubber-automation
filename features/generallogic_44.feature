Feature: Scrubber General Logic_44 When +99100 is not entered with Anesthesia code.
  
  Scenario: Logic 44 Case1 When +99100 is not entered with Anesthesia code 00902 and Age of patient is 8 months
	Given Logic 44 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic44 case1
	Then I should validate following for Logic44 case1
		| row | code | severity | message |
		| 1 | 00902 | MEDIUM | Consider reporting Procedure code +99100 in addition to primary anesthesia code.|
		
  Scenario: Logic 44 Case2 Primary anesthesia code 00145 is entered with +99100 code and age of the patient is 70+ years.
	Given Logic 44 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic44 case2
	Then I should validate following for Logic44 case2
		| row | code | severity | message |
		| 1 | 00145 | MEDIUM | Consider reporting Procedure code +99100 in addition to primary anesthesia code.|
		
  Scenario: Logic 44 Case3 Primary anesthesia code 00147 is entered without +99100 code and age of the patient is 50 years
	Given Logic 44 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic44 case3
	Then I should validate following for Logic44 case3
		| row | code | severity | message |
		| 1 | 00147 | MEDIUM | Consider reporting Procedure code +99100 in addition to primary anesthesia code.|
		
  Scenario: Logic 44 Case4 Anesthesia code Px 00561 is entered without +99100 code and age of the patient is 11 months 30 days
	Given Logic 44 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic44 case4
	Then I should validate following for Logic44 case4
		| row | code | severity | message |
		| 1 | 00561 | MEDIUM | Consider reporting Procedure code +99100 in addition to primary anesthesia code.|
		
  Scenario: Logic 44 Case5 Primary anesthesia code 00147 is entered without +99100 code and age of the patient is 80 years
	Given Logic 44 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic44 case5
	Then I should validate following for Logic44 case5
		| row | code | severity | message |
		| 1 | 00147 | MEDIUM | Consider reporting Procedure code +99100 in addition to primary anesthesia code.|