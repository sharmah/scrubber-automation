Feature: Scrubber General Logic_122 When LCD L36286 applies to claim have CPT 15820 or 15821.

  Scenario: Logic 122 Case1 When CPT 15820 is entered
    Given Logic 122 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic122 case1
	Then I should validate following for Logic122 case1
		| row | code | severity | message |
		| 1 | 15820 | CRITICAL | Noridian considers lower eyelid blepharoplasty to be a non-covered service under LCD L36286. You may appeal on a case-by-case basis.|
		
  Scenario: Logic 122 Case2 When CPT 15821 is entered
    Given Logic 122 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic122 case2
	Then I should validate following for Logic122 case2
		| row | code | severity | message |
		| 1 | 15821 | CRITICAL | Noridian considers lower eyelid blepharoplasty to be a non-covered service under LCD L36286. You may appeal on a case-by-case basis.|
		
  Scenario: Logic 122 Case3 When CPT 15822 and 15820 used with MOD RT and LT respectively.
    Given Logic 122 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic122 case3
	Then I should validate following for Logic122 case3
		| row | code | severity | message |
		| 1 | 15820 | CRITICAL | Noridian considers lower eyelid blepharoplasty to be a non-covered service under LCD L36286. You may appeal on a case-by-case basis.|