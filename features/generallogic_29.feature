Feature: Scrubber General Logic_29 Modifier 24 is associated to a CPT code other than E/M code.
  
  Scenario: Logic 29 Case1 Modifier 24 is associated to a CPT code other than E/M code.
	Given Logic 29 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic29 case1
	Then I should validate following for Logic29 case1
		| row | code | severity | message |
		| 1 | 69210 | CRITICAL | Invalid modifier: Modifier 24 can be used only with E/M procedure codes|
		
  Scenario: Logic 29 Case2 Modifier 24 is associated to an E/M code.
	Given Logic 29 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic29 case2
	Then I should validate following for Logic29 case2
		| row | code | severity | message |
		| 1 | 99203 | CRITICAL | Invalid modifier: Modifier 24 can be used only with E/M procedure codes|
		
  Scenario: Logic 29 Case3 Modifier 24 is associated to a CPT code and E/M code both.
	Given Logic 29 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic29 case3
	Then I should validate following for Logic29 case3
		| row | code | severity | message |
		| 1 | 69210 | CRITICAL | Invalid modifier: Modifier 24 can be used only with E/M procedure codes|