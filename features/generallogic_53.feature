Feature: Scrubber General Logic_53 When no LCD policy available under this state and contractor, for this CPT/HCPCS code. Dx may not match the procedure

  Scenario: Logic 53 Case1 When LCD policy available under this state and contractor, for this CPT/HCPCS code.
    Given Logic 53 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic53 case1
	Then I should validate following for Logic53 case1
		| row | code | severity | message |
		| 1 | H05.51 | LOW | There is no LCD policy available under this state and contractor, for this CPT/HCPCS code. Diagnosis code may not match/support the procedure|
		
  Scenario: Logic 53 Case2 When no LCD policy available under this state and contractor, for this CPT/HCPCS code. Dx may not match the procedure
	Given Logic 53 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic53 case2
	Then I should validate following for Logic53 case2
		| row | code | severity | message |
		| 1 | G89.0 | LOW | There is no LCD policy available under this state and contractor, for this CPT/HCPCS code. Diagnosis code may not match/support the procedure|
		
  Scenario: Logic 53 When LCD policy available under this state and contractor, for this CPT/HCPCS code.
	Given Logic 53 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic53 case3
	Then I should validate following for Logic53 case3
		| row | code | severity | message |
		| 1 | G89.0 | LOW | There is no LCD policy available under this state and contractor, for this CPT/HCPCS code. Diagnosis code may not match/support the procedure|
		
Scenario: Logic 53 Case4 When there are two Px and LCD policy available under this state and contractor, for one CPT/HCPCS code and not for other.
	Given Logic 53 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic53 case4
	Then I should validate following for Logic53 case4
		| row | code | severity | message |
		| 1 | H22 | LOW | There is no LCD policy available under this state and contractor, for this CPT/HCPCS code. Diagnosis code may not match/support the procedure|
