Feature: Scrubber General Logic_236 When L33927 applies to the claim and User enters CPT 92136 with modifier 26 And Doesn’t append modifier LT, RT to 92136-26.

  Scenario: Logic 236 Case1 When CPT 92136 with modifier 26 And Doesn’t append modifier LT, RT to 92136-26.
    Given Logic 236 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic236 case1
	Then I should validate following for Logic236 case1
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Laterality modifier is missing. You must append LT or RT to 92136-26 (per Coding Guidelines attached with LCD L33927).|
		
  Scenario: Logic 236 Case2 When CPT 92136 with modifier 26 And append modifier LT, RT to 92136-26.
    Given Logic 236 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic236 case2
	Then I should validate following for Logic236 case2
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Laterality modifier is missing. You must append LT or RT to 92136-26 (per Coding Guidelines attached with LCD L33927).|