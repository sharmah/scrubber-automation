Feature: Scrubber General Logic_39 CPT Other than Anesthesia codes are entered with MOD P1/ P2/ P3/ P4/ P5/ P6/AA/QX/QY/QZ/G8/G9/AD/QK 
  
  Scenario: Logic 39  CPT 92018 is entered with MOD P1/ P2/ P3/ P4/ P5/ P6/AA/QX/QY/QZ/G8/G9/AD/QK 
	Given Logic 39 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic39 case1
	Then I should validate following for Logic39 case1
		| row | code | severity | message |
		| 1 | 92018 | MEDIUM | Anesthesia modifier AA - cannot be appended to this non-anesthesia procedure code|
		
  Scenario: Logic 39 HCPCS G0186 is entered with MOD P1/ P2/ P3/ P4/ P5/ P6/AA/QX/QY/QZ/G8/G9/AD/QK. 
	Given Logic 39 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic39 case2
	Then I should validate following for Logic39 case2
		| row | code | severity | message |
		| 1 | G0186 | MEDIUM | Anesthesia modifier P3 - cannot be appended to this non-anesthesia procedure code|
		
  Scenario: Logic 39 Case3 Anesthesia code 00140 is entered with Mod P3
	Given Logic 39 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic39 case3
	Then I should validate following for Logic39 case3
		| row | code | severity | message |
		| 1 | 00140 | MEDIUM | Anesthesia modifier P3 - cannot be appended to this non-anesthesia procedure code|
		