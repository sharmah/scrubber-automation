Feature: Scrubber General Logic_243 When L33927 applies to the claim and CPT 92136 appended with Mod RT or LT (not both) and Mod TC and 26 not appended.

  Scenario: Logic 243 Case1 When CPT 92136 appended with Mod RT or LT (not both) and Mod TC and 26 not appended.
    Given Logic 243 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic243 case1
	Then I should validate following for Logic243 case1
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Do not bill site modifiers (RT, LT) with 92136. 92136 is a bilateral service, and reimbursement is based on the total service for both eyes (per Coding Guidelines attached with LCD L33927).|