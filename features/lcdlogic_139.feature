Feature: Scrubber General Logic_139 When L33584 applies to the claim, patient is younger than 65 years and CPT is 0308T.

  Scenario: Logic 139 Case1 When L33584 applies to the claim, patient is younger than 65 years and CPT is 0308T.
    Given Logic 139 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic139 case1
	Then I should validate following for Logic139 case1
		| row | code | severity | message |
		| 1 | 0308T | CRITICAL | Confirm patient DOB. “The intraocular telescope is indicated for monocular implantation to improve vision in patients who are greater than or equal to 65 years of age” (LCD L33584).|
		
  Scenario: Logic 139 Case2 When L33584 applies to the claim, patient is older than 65 years and CPT is 0308T.
    Given Logic 139 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic139 case2
	Then I should validate following for Logic139 case2
		| row | code | severity | message |
		| 1 | 0308T | CRITICAL | Confirm patient DOB. “The intraocular telescope is indicated for monocular implantation to improve vision in patients who are greater than or equal to 65 years of age” (LCD L33584).|