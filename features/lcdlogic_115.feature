Feature: Scrubber General Logic_115 When LCD L33567 applies to the claim and primary cpt 92226 is used with CPT 92002 or 92004.

  Scenario: Logic 115 Case1 When 92226 and 92002 comes in one claim.
    Given Logic 115 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic115 case1
	Then I should validate following for Logic115 case1
		| row | code | severity | message |
		| 1 | 92002 | CRITICAL | NGS does not consider 92002 payable with 92226 (NGS Article A52861).|
		
  Scenario: Logic 115 Case2 When 92225 and 92004 comes in one claim.
    Given Logic 115 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic115 case2
	Then I should validate following for Logic115 case2
		| row | code | severity | message |
		| 1 | 92004 | CRITICAL | NGS does not consider 92004 payable with 92225 (NGS Article A52861).|