Feature: Scrubber General Logic_232 When LCD L34008 applies to the claim and CPT 92025 linked to ICD-10 code 1 Z96.1, Z98.41, and Z98.42.and it is not accompanied by H52.211, H52.212, or H52.213. NOTE- ICD-10 code can be on any position.

  Scenario: Logic 232 Case1 When CPT 92025 linked to ICD-10 code 1 Dx Z96.1 and Dx H52.211, H52.212, or H52.213 is not linked.
    Given Logic 232 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic232 case1
	Then I should validate following for Logic232 case1
		| row | code | severity | message |
		| 1 | Z96.1 | CRITICAL | Code H52.211, H52.212, or H52.21 must accompany Z96.1 when reported with 92025 (LCD L34008).|
		
  Scenario: Logic 232 Case2 When CPT 92025 linked to ICD-10 code 1 Dx Z96.1 and Dx H52.211, H52.212, or H52.213 is also linked.
    Given Logic 232 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic232 case2
	Then I should validate following for Logic232 case2
		| row | code | severity | message |
		| 1 | Z96.1 | CRITICAL | Code H52.211, H52.212, or H52.21 must accompany Z96.1 when reported with 92025 (LCD L34008).|