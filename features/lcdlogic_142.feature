Feature: Scrubber General Logic_142 When L35008 applies to the claim and user enters amount more than $0.01 as charge for CPT (Group2)

  Scenario: Logic 142 Case1 When L35008 applies to the claim and user enters amount more than $0.01 as charge for CPT (Group2)
    Given Logic 142 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic142 case1
	Then I should validate following for Logic142 case1
		| row | code | severity | message |
		| 1 | 0437T | CRITICAL | 0437T is not separately billable (LCD L35008). Noridian groups the code under the header “Components of another service, never separately billable to the contractor or patient.|