Feature: Scrubber General Logic_113 When L33567 applies to claim, CPT entered is 92225 or 92226 and no modifier RT, LT or 50 is appended.

  Scenario: Logic 113 Case1 Claim have all required points to flag an error message but NGS Article A52861 is not active on given DOS.
    Given Logic 113 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic113 case1
	Then I should validate following for Logic113 case1
		| row | code | severity | message |
		| 1 | 92225 | CRITICAL | Append modifier RT, LT, or 50 to 92225 to indicate eye(s) involved. NGS will return the claim if there is no modifier (NGS Article A52861).|
		
  Scenario: Logic 113 Case2 When L33567 applies to claim, CPT entered is 92225 or 92226 and no modifier RT, LT or 50 is appended. Article A53060 is active.
	When I hit the API with username and password with valid EDI for Logic113 case2
	Then I should validate following for Logic113 case2
		| row | code | severity | message |
		| 1 | 92225 | CRITICAL | Append modifier RT, LT, or 50 to 92225 to indicate eye(s) involved. NGS will return the claim if there is no modifier (NGS Article A52861).|
		
  Scenario: Logic 113 Case3 When L33567 applies to claim, CPT entered is 92225 or 92226 and modifier 50 is appended
    Given Logic 113 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic113 case3
	Then I should validate following for Logic113 case3
		| row | code | severity | message |
		| 1 | 92226 | CRITICAL | Append modifier RT, LT, or 50 to 92226 to indicate eye(s) involved. NGS will return the claim if there is no modifier (NGS Article A52861).|
		
  Scenario: Logic 113 Case4 When L33567 applies to claim, CPT entered is 92225 or 92226 and modifier RT/LT is appended
    Given Logic 113 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic113 case4
	Then I should validate following for Logic113 case4
		| row | code | severity | message |
		| 1 | 92226 | CRITICAL | Append modifier RT, LT, or 50 to 92226 to indicate eye(s) involved. NGS will return the claim if there is no modifier (NGS Article A52861).|