Feature: Scrubber General Logic_230 When L34017 applies to the claim and User enters CPT 92225 AND 92226 with mod RT/LT/50. Both the codes have same eye and same DOS.

  Scenario: Logic 230 Case1 When CPT 92225 AND 92226 with mod RT/LT/50. Both the codes have same eye and same DOS.
    Given Logic 230 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic230 case1
	Then I should validate following for Logic230 case1
		| row | code | severity | message |
		| 1 | 92226 | CRITICAL | FCSO will not reimburse 92225 and 92226 on the same day for the same eye by the same provider (LCD L34017, coding guidelines attachment).|