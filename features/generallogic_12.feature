Feature: Scrubber General Logic_12 CCI edit col1 and col2 codes are mutually exclusive.
  
  Scenario: Logic 12 Case1 CCI edit col1 and col2 codes are mutually exclusive.
	Given Logic 12 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic12 case1
	Then I should validate following for logic12 case1
		| row | code | severity | message |
		| 1 | Q2034 | CRITICAL | CCI edit: This procedure code can never be reported together with code Q2039 due to CCI mutually exclusive edit |
		
  Scenario: Logic 12 Case2 CCI edit col1 and col2 codes are mutually exclusive- One of the Px is deleted
	Given Logic 12 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic12 case2
	Then I should validate following for logic11 case2
		| row | code | severity | message |
		| 1 | 47136 | CRITICAL | CCI edit: This procedure code can never be reported together with code 47135 due to CCI mutually exclusive edit|
		
  Scenario: Logic 12 Case3 CCI edit col1 and col2 codes are mutually exclusive- Px combi is effective from April, 2016
	Given Logic 12 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic12 case3
	Then I should validate following for logic12 case3
		| row | code | severity | message |
		| 1 | 99363 | CRITICAL | CCI edit: This procedure code can never be reported together with code G0250 due to CCI mutually exclusive edit|
		
  Scenario: Logic 12 Case4 CCI edit col1 and col2 codes are mutually exclusive - DOS after April, 2016.
	Given Logic 12 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic12 case4
	Then I should validate following for logic12 case4
		| row | code | severity | message |
		| 1 | 99363 | CRITICAL | CCI edit: This procedure code can never be reported together with code G0250 due to CCI mutually exclusive edit|
		
  