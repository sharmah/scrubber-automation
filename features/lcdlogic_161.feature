Feature: Scrubber General Logic_161 When LCD L33574 applies to the claim and <code>, which is 92081-26, 92082-26, or 92083-26 and POS is NOT 11, 21, 22, 23, 31, 32, 49, or 62 and DOS for <code> is on or between Jan. 1, 2016, and May 1, 2016.

  Scenario: Logic 161 Case1 When CPT 92081-26 is entered, POS is 18 and DOS is between Jan. 1, 2016, and May 1, 2016.
    Given Logic 161 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic161 case1
	Then I should validate following for Logic161 case1
		| row | code | severity | message |
		| 1 | 18 | CRITICAL | Confirm POS. NGS does not list POS 18 as a payable POS for 92081 (NGS Article A52829).|
		
  Scenario: Logic 161 Case2 When CPT 92081-26 is entered, POS is 22 and DOS is between Jan. 1, 2016, and May 1, 2016.
    Given Logic 161 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic161 case2
	Then I should validate following for Logic161 case2
		| row | code | severity | message |
		| 1 | 22 | CRITICAL | Confirm POS. NGS does not list POS 22 as a payable POS for 92081 (NGS Article A52829).|
		
  Scenario: Logic 161 Case3 When CPT 92081-26 is entered, POS is 18 and DOS is after May 1, 2016.
    Given Logic 161 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic161 case3
	Then I should validate following for Logic161 case3
		| row | code | severity | message |
		| 1 | 18 | CRITICAL | Confirm POS. NGS does not list POS 18 as a payable POS for 92081 (NGS Article A52829).|