Feature: Scrubber General Logic_18 Code1 and code2 lie in some specific range.
  
  Scenario: Logic 18 Case1 Code1 and code2 lie in some specific range.
	Given Logic 18 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic18 case1
	Then I should validate following for Logic18 case1
		| row | code | severity | message |
		| 1 | 99468 | MEDIUM | Use modifier 25 if 99468 represents a significant and separately identifiable E/M service beyond 99462|
		
  Scenario: Logic 18 Case2 Code1 and code2 lie in some specific range. Mod 25 appended
	Given Logic 18 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic18 case2
	Then I should validate following for Logic18 case2
		| row | code | severity | message |
		| 1 | 99468 | MEDIUM | Use modifier 25 if 99468 represents a significant and separately identifiable E/M service beyond 99462|