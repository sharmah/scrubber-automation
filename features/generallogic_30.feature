Feature: Scrubber General Logic_30 DOS entered is not in YYYY-MM-DD format.
  
  Scenario: Logic 30 Case1 DOS entered is not in YYYYMMDD format
	Given Logic 30 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic30 case1
	Then I should validate following for Logic30 case1
		| row | code | severity | message |
		| 1 | 69210 | CRITICAL | Invalid date: please enter proper DOS in MM-DD-YYYY format|