Feature: Scrubber General Logic_98 When ICD-10 code linked to CPT code 66982, LCD L33558 applies to the  claim and none of the ICD lie under Group1 ICD-10 Codes that Support Medical Necessity in L33558."

  Scenario: Logic 98 Case1 When Dx H22 (not from group1) is entered.
    Given Logic 98 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic98 case1
	Then I should validate following for Logic98 case1
		| row | code | severity | message |
		| 1 | H22 | CRITICAL | Confirm primary diagnosis code choice. NGS does not list H22 as an appropriate primary diagnosis code for 66982 (LCD L33538)|
		
  Scenario: Logic 98 Case2 When Dx Q12.0 (from group1) is entered.
    Given Logic 98 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic98 case2
	Then I should validate following for Logic98 case2
		| row | code | severity | message |
		| 1 | Q12.0 | CRITICAL | Confirm primary diagnosis code choice. NGS does not list Q12.0 as an appropriate primary diagnosis code for 66982 (LCD L33538)|
		
  Scenario: Logic 98 Case3 When PDx H22 (not from group1) and SDx Q12.0 (from group1) is entered.
    Given Logic 98 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic98 case3
	Then I should validate following for Logic98 case3
		| row | code | severity | message |
		| 1 | H22 | CRITICAL | Confirm primary diagnosis code choice. NGS does not list H22 as an appropriate primary diagnosis code for 66982 (LCD L33538)|