Feature: Scrubber General Logic_41 Mod 73 is used with the Px
  
  Scenario: Logic 41 Case1 Mod 73 is used with the Px
	Given Logic 41 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic41 case1
	Then I should validate following for Logic41 case1
		| row | code | severity | message |
		| 1 | 65855 | CRITICAL | Modifier 73 is for use by the outpatient hospital/ASC facility. Physicians should see modifiers 52 and 53 for reduced/discontinued procedures.|
		
  Scenario: Logic 41 Case2 Mod 73 and 50 is used with the Px
	Given Logic 41 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic41 case2
	Then I should validate following for Logic41 case2
		| row | code | severity | message |
		| 1 | 65855 | CRITICAL | Modifier 73 is for use by the outpatient hospital/ASC facility. Physicians should see modifiers 52 and 53 for reduced/discontinued procedures.|
		
  Scenario: Logic 41 Case3 Mod 73 is used with the Px
	Given Logic 41 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic41 case3
	Then I should validate following for Logic41 case3
		| row | code | severity | message |
		| 1 | 65860 | CRITICAL | Modifier 73 is for use by the outpatient hospital/ASC facility. Physicians should see modifiers 52 and 53 for reduced/discontinued procedures.|