Feature: Scrubber General Logic_37 Mod 66 is entered and  modifier indicator for that CPT code is either 0 or 9.
  
  Scenario: Logic 37 Mod 66 is entered and  modifier indicator for that CPT code is either 0 or 9.
	Given Logic 37 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic37 case1
	Then I should validate following for Logic37 case1
		| row | code | severity | message |
		| 1 | S2235 | MEDIUM | Modifier 66 cannot be appended to this code: Team surgeons not permitted for this procedure|
		
  Scenario: Logic 37 Case2 Mod 66 is entered and  modifier indicator for that CPT code is not 0 or 9.
	Given Logic 37 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic37 case2
	Then I should validate following for Logic37 case2
		| row | code | severity | message |
		| 1 | 68399 | MEDIUM | Modifier 66 cannot be appended to this code: Team surgeons not permitted for this procedure|
		
  Scenario: Logic 37 Case3 Mod 66 and some other Mod is entered and modifier indicator for that CPT code is not 0 or 9.
	Given Logic 37 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic37 case3
	Then I should validate following for Logic37 case3
		| row | code | severity | message |
		| 1 | S2235 | MEDIUM | Modifier 66 cannot be appended to this code: Team surgeons not permitted for this procedure|
		
  Scenario: Logic 37 Case4 Mod 66 with the code and modifier indicator for that CPT code is either 0 or 9.
	Given Logic 37 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic37 case4
	Then I should validate following for Logic37 case4
		| row | code | severity | message |
		| 1 | 65093 | MEDIUM | Modifier 66 cannot be appended to this code: Team surgeons not permitted for this procedure|