Feature: Scrubber General Logic_65 when <Code 2> is included in the global surgical package of <Code 1> and the code represents an unrelated E/M service by the same provider during the postoperative period.

  Scenario: Logic 65 Case1 
    Given Logic 65 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic65 case1
	Then I should validate following for Logic65 case1
		| row | code | severity | message |
		| 1 | 99213 | MEDIUM | 99213 is included in the global surgical package of 65855 Append modifier 24 to 99213 when the code represents an unrelated E/M service by the same provider during the postoperative period.|
		
  Scenario: Logic 65 Case2 
	Given Logic 65 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic65 case2
	Then I should validate following for Logic65 case2
		| row | code | severity | message |
		| 1 | 99214 | MEDIUM | 99214 is included in the global surgical package of 65855 Append modifier 24 to 99214 when the code represents an unrelated E/M service by the same provider during the postoperative period.|
		
  Scenario: Logic 65 Case3 
    Given Logic 65 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic65 case3
	Then I should validate following for Logic65 case3
		| row | code | severity | message |
		| 1 | 99213 | MEDIUM | 99213 is included in the global surgical package of 65855 Append modifier 24 to 99213 when the code represents an unrelated E/M service by the same provider during the postoperative period.|
		
  Scenario: Logic 65 Case4
  Given Logic 65 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic65 case4
	Then I should validate following for Logic65 case4
		| row | code | severity | message |
		| 1 | 99212 | MEDIUM | 99212 is included in the global surgical package of 65920 Append modifier 24 to 99212 when the code represents an unrelated E/M service by the same provider during the postoperative period.|
		
  Scenario: Logic 65 Case5 
	Given Logic 65 Case5 valid EDI
	When I hit the API with username and password with valid EDI for Logic65 case5
	Then I should validate following for Logic65 case5
		| row | code | severity | message |
		| 1 | 99213 | MEDIUM | 99213 is included in the global surgical package of 65920. Append modifier 24 to 99213 when the code represents an unrelated E/M service by the same provider during the postoperative period.|

  Scenario: Logic 65 Case6 
  Given Logic 65 Case6 valid EDI
	When I hit the API with username and password with valid EDI for Logic65 case6
	Then I should validate following for Logic65 case6
		| row | code | severity | message |
		| 1 | 99212 | MEDIUM | 99212 is included in the global surgical package of 66030 Append modifier 24 to 99212 when the code represents an unrelated E/M service by the same provider during the postoperative period.|
		
  Scenario: Logic 65 Case7
    Given Logic 65 Case7 valid EDI
	When I hit the API with username and password with valid EDI for Logic65 case7
	Then I should validate following for Logic65 case7
		| row | code | severity | message |
		| 1 | 99212 | MEDIUM | 99212 is included in the global surgical package of 66030 Append modifier 24 to 99212 when the code represents an unrelated E/M service by the same provider during the postoperative period.|
