Feature: Scrubber General Logic_54 When no LCD policy available under this state and contractor, for this CPT/HCPCS code. Dx match/support the procedure

  Scenario: Logic 54 Case1 When no LCD policy available under this state and contractor, for this CPT/HCPCS code. Dx match/support the procedure
    Given Logic 54 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic54 case1
	Then I should validate following for Logic54 case1
		| row | code | severity | message |
		| 1 | H44.753 | LOW | There is no LCD policy available under this state and contractor, for this CPT/HCPCS code. However the Diagnosis code matches/supports the procedure.|
		
  Scenario: Logic 54 Case2 When LCD policy available under this state and contractor, for this CPT/HCPCS code. 
	Given Logic 54 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic54 case2
	Then I should validate following for Logic54 case2
		| row | code | severity | message |
		| 1 | E09.36| LOW | There is no LCD policy available under this state and contractor, for this CPT/HCPCS code. However the Diagnosis code matches/supports the procedure.|
		
  Scenario: Logic 54 Case3 When no LCD policy available under this state and contractor, for this CPT/HCPCS code. Dx match/support the procedure
	Given Logic 54 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic54 case3
	Then I should validate following for Logic54 case3
		| row | code | severity | message |
		| 1 | H21.40 | LOW | There is no LCD policy available under this state and contractor, for this CPT/HCPCS code. However the Diagnosis code matches/supports the procedure.|