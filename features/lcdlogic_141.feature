Feature: Scrubber General Logic_141 When L35008 applies to the claim and no ABN modifier is associated to the Group 1 code under CPT/HCPCS Codes in L35008.

  Scenario: Logic 141 Case1 When L35008 applies to the claim and no ABN modifier is associated to the CPT (Group 1) 
    Given Logic 141 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic141 case1
	Then I should validate following for Logic141 case1
		| row | code | severity | message |
		| 1 | J2010 | CRITICAL | J2010 is a non-covered service per LCD L35008. Noridian considers the service not proven effective or not medically reasonable and necessary and will deny the code. Append GA if an ABN is on file. Append GZ if an ABN is not on file|
		
  Scenario: Logic 141 Case2 When L35008 applies to the claim and no ABN modifier is associated to the HCPCS (Group 1)
    Given Logic 141 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic141 case2
	Then I should validate following for Logic141 case2
		| row | code | severity | message |
		| 1 | 97026 | CRITICAL | 97026 is a non-covered service per LCD L35008. Noridian considers the service not proven effective or not medically reasonable and necessary and will deny the code. Append GA if an ABN is on file. Append GZ if an ABN is not on file|