Feature: Scrubber General Logic_27 modifier associated to a CPT does not match.
  
  Scenario: Logic 27 Case1 Modifier associated to a CPT does not match.
	Given Logic 27 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic27 case1
	Then I should validate following for Logic27 case1
		| row | code | severity | message |
		| 1 | 92225 | LOW | PO : This modifier usually is not associated with this Procedure code|
		
  Scenario: Logic 27 Case2 Modifier associated to a CPT does match.
	Given Logic 27 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic27 case2
	Then I should validate following for Logic27 case2
		| row | code | severity | message |
		| 1 | 92225 | LOW | 22 : This modifier usually is not associated with this Procedure code|
		
  Scenario: Logic 27 Case3 Modifier associated to a HCPCS does not match.
	Given Logic 27 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic27 case3
	Then I should validate following for Logic27 case3
		| row | code | severity | message |
		| 1 | V2520 | LOW | RR : This modifier usually is not associated with this Procedure code|
		
  Scenario: Logic 27 Case4 Modifier associated to a HCPCS does not match.
	Given Logic 27 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic27 case4
	Then I should validate following for Logic27 case4
		| row | code | severity | message |
		| 1 | V2520 | LOW | FB : This modifier usually is not associated with this Procedure code|