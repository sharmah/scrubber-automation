Feature: Scrubber General Logic_86 when claim have CPT in group 1-10 and HCPCS from in group 2 and L33458 applies to the claim.

  Scenario: Logic 86 Case1 When claim have CPT in group 1-10 and HCPCS from in group 2 and L33458 applies to the claim.
    Given Logic 86 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic86 case1
	Then I should validate following for Logic86 case1
		| row | code | severity | message |
		| 1 | 52287 | MEDIUM | LCD L33458 pairs 52287 with botulinumtoxin code(s) Array. Confirm your HCPCS code choice J0586.|
		
  Scenario: Logic 86 Case2 When both CPT and HCPCS lie in same group.
    Given Logic 86 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic86 case2
	Then I should validate following for Logic86 case2
		| row | code | severity | message |
		| 1 | 52287 | MEDIUM | LCD L33458 pairs 52287 with botulinumtoxin code(s) Array. Confirm your HCPCS code choice J0585.|