Feature: Scrubber General Logic_147  When LCD L35098 applies to the claim, User enters Billing Specialty code 65. And User enters CPT 95905.

  Scenario: Logic 147 Case1 When Billing provider speciality is 65.
    Given Logic 147 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic147 case1
	Then I should validate following for Logic147 case1
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | NGS doesn’t allow 95905 for physical therapists (LCD L35098).|
		
  Scenario: Logic 147 Case2 When Billing provider speciality is not 65.
    Given Logic 147 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic147 case2
	Then I should validate following for Logic147 case2
		| row | code | severity | message |
		| 1 | 11 | CRITICAL | NGS doesn’t allow 95905 for physical therapists (LCD L35098).|