Feature: Scrubber General Logic_167 When LCD L34171 applies to the claim having CPT <code> 68801, 68810, or 68840 and POS is NOT 11, 13, 21, 22, 23, 24, 31, 32, 33, or 49.

  Scenario: Logic 167 Case1 When LCD L34171 applies to the claim having CPT <code> 68801, 68810, or 68840 and POS is NOT 11, 13, 21, 22, 23, 24, 31, 32, 33, or 49.
    Given Logic 167 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic167 case1
	Then I should validate following for Logic167 case1
		| row | code | severity | message |
		| 1 | 12 | CRITICAL | Confirm POS. CGS does not list POS 12 as a payable POS for 68801 (CGS Article A52391) .|