Feature: Scrubber General Logic_239 When L33927 applies to the claim and User enters CPT 92136 without modifier 26 And TC and append modifier LT or RT or both to 92136.

  Scenario: Logic 239 Case1 When CPT 92136 without modifier 26 And TC and append modifier LT or RT or both to 92136.
    Given Logic 239 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic239 case1
	Then I should validate following for Logic239 case1
		| row | code | severity | message |
		| 1 | 92136 | CRITICAL | Don’t use modifiers LT and RT to indicate services performed bilaterally on the same date of service. Global 92136 is a bilateral service, and reimbursement is based on the total service for both eyes (per Coding Guidelines attached with LCD L33927).|