Feature: Scrubber General Logic_182 When LCD L36232 applies to the claim and DOS is on or after 11/22/2015 and User enters 68761 and
Modifier code E1, E2, E3 or E4 is not appended to CPT 68761.


  Scenario: Logic 182 Case1 When CPT - 68761 and No Mod is entered.
    Given Logic 182 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic182 case1
	Then I should validate following for Logic182 case1
		| row | code | severity | message |
		| 1 | 68761 | CRITICAL | Confirm modifier choice with 68761. You should apply modifier E1, E2, E3, or E4 to 68761 (First Coast Article A54680).|
		
  Scenario: Logic 182 Case2 When CPT - 68761 and Mod E1 is entered.
    Given Logic 182 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic182 case2
	Then I should validate following for Logic182 case2
		| row | code | severity | message |
		| 1 | 68761 | CRITICAL | Confirm modifier choice with 68761. You should apply modifier E1, E2, E3, or E4 to 68761 (First Coast Article A54680).|
		
  Scenario: Logic 182 Case3 When CPT - 68761 and No Mod is entered and DOS entered is before 11/22/2015
    Given Logic 182 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic182 case3
	Then I should validate following for Logic182 case3
		| row | code | severity | message |
		| 1 | 68761 | CRITICAL | Confirm modifier choice with 68761. You should apply modifier E1, E2, E3, or E4 to 68761 (First Coast Article A54680).|