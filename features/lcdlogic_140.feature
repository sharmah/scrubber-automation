Feature: Scrubber General Logic_140 When history already have an existing similar claim on prior DOS

  Scenario: Logic 140 Case1 When this claim is a new claim and no such claim exist in history.
    Given Logic 140 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic140 case1
	Then I should validate following for Logic140 case1
		| row | code | severity | message |
		| 1 | 0308T | CRITICAL | This patient has a prior 0308T claim. Confirm code choice. The service is indicated for implant in only one eye (LCD L33584).|
		
  Scenario: Logic 140 Case2 When this claim is similar to the one exist in DB.
    Given Logic 140 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic140 case2
	Then I should validate following for Logic140 case2
		| row | code | severity | message |
		| 1 | 0308T | CRITICAL | This patient has a prior 0308T claim. Confirm code choice. The service is indicated for implant in only one eye (LCD L33584).|