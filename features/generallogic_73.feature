Feature: Scrubber General Logic_73 A Subsequent code is entered in a claim without any availability of initial visit claim in the history.

  Scenario: Logic 73 Case1 Subsequent code 99307 is entered in a claim without any availability of initial visit claim in the history.
    Given Logic 73 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic73 case1
	Then I should validate following for Logic73 case1
		| row | code | severity | message |
		| 1 | 99307 | MEDIUM | Confirm subsequent visit E/M code is appropriate. No initial visit claim is in this system. The subsequent code may be appropriate if the provider (or one from the same group/same specialty) performed an initial visit that is not in the system or (for payers that follow Medicare rules) if the initial visit did not meet initial visit code requirements.|
		
  Scenario: Logic 73 Case2 When initial code 99222 is entered.
	Given Logic 73 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic73 case2
	Then I should validate following for Logic73 case2
		| row | code | severity | message |
		| 1 | 99222 | MEDIUM | Confirm subsequent visit E/M code is appropriate. No initial visit claim is in this system. The subsequent code may be appropriate if the provider (or one from the same group/same specialty) performed an initial visit that is not in the system or (for payers that follow Medicare rules) if the initial visit did not meet initial visit code requirements.|
		
  Scenario: Logic 73 Case4 When modifier 59 is entered against a procedure code
    Given Logic 73 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic73 case4
	Then I should validate following for Logic73 case4
		| row | code | severity | message |
		| 1 | 71021 | LOW | Use precise alternative to modifier 59: You may want to report modifiers XE/XP/XS/XU replacing 59 in specific situations such as separate encounter, separate practitioner, separate site or unusual non-overlapping service. Modifier 59 should not be used when a more descriptive modifier is available. CMS will continue to recognize the modifier 59 in many instances but may selectively require a more specific -X(EPSU) modifier for billing certain codes at high risk for incorrect billing.|