Feature: Scrubber General Logic_168 When LCD L34171 applies to the claim having CPT <code> 68811 or 68815 and POS is NOT 11, 21, 22, 23, 24, or 49.

  Scenario: Logic 168 Case1 When LCD L34171 applies to the claim having CPT <code> 68811 or 68815 and POS is NOT 11, 21, 22, 23, 24, or 49.
  
    Given Logic 168 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic168 case1
	Then I should validate following for Logic168 case1
		| row | code | severity | message |
		| 1 | 12 | CRITICAL | Confirm POS. CGS does not list POS 12 as a payable POS for 68811 (CGS Article A52391) .|