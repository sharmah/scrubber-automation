Feature: Scrubber General Logic_257 When LCD L33944 applies to the claim and CPT Codes from Group 2 CPT Codes under CPT/HCPCS Codes in L33944

  Scenario: Logic 257 Case1 When CPT 15820 (grp1) from Group 2 CPT Codes under CPT/HCPCS Codes in L33944 is entered.
    Given Logic 257 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic257 case1
	Then I should validate following for Logic257 case1
		| row | code | severity | message |
		| 1 | 15820 | CRITICAL | CGS may consider 15820 to be cosmetic and thus not covered by Medicare. “Documentation to support functional impairment, visual or otherwise must be present” (LCD L33944).|