$response = ""

Given (/^Logic 64 Case1 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic64\\Logic64_Case1.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic64 case1$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic64 case1$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 64 Case2 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic64\\Logic64_Case2.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic64 case2$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic64 case2$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 64 Case3 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic64\\Logic64_Case3.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic64 case3$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic64 case3$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 64 Case4 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic64\\Logic64_Case4.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic64 case4$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic64 case4$/) do |edi_values_to_check|
	case_truthness = true
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = false
		end
	end
	assert case_truthness
end

Given (/^Logic 64 Case5 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic64\\Logic64_Case5.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic64 case5$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic64 case5$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end

Given (/^Logic 64 Case6 valid EDI$/) do
	file = File.open("D:\\Khushboo\\Cucumber\\scrubber\\features\\edi_files\\Logic64\\Logic64_Case6.txt", "rb")
	$edi_data = file.read
end

When(/^I hit the API with username and password with valid EDI for Logic64 case6$/) do 
	$response = RestClient.post $api_url, { 'requestData' => $edi_data }
end

Then(/^I should validate following for Logic64 case6$/) do |edi_values_to_check|
	case_truthness = false
	scrubber_response = JSON.parse($response)
	puts "\r\n"
	scrubber_response["results"][0]["claim"]["claimResults"].each_with_index do |(v),i|
		#puts "\r\n now it is the expectation#{i}"
		#puts "\r\n"
		v["row"] = "1"
		Expected =  edi_values_to_check.hashes
		finalExpected =  Expected.to_s
		#puts "\r\n"
		#puts v
		v_response = "[#{v}]";
		#puts "\r\n"
		#puts v_response
		#puts "\r\n"
		#puts finalExpected
		if finalExpected == v_response
			case_truthness = true
		end
	end
	assert case_truthness
end
