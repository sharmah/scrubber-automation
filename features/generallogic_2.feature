Feature: Scrubber General Logic_2
  
  Scenario: Logic 2 Case1 Require Additional Digit- 4th Digit missing
	Given Logic 2 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic2 case1
	Then I should validate following for logic2 case1
		| row | code | severity | message |
		| 1 | R10 | CRITICAL | Diagnosis code requires additional (4th,5th, 6th or 7th ) digit.|
		
  Scenario: Logic 2 Case2 Require Additional Digit- No 5th digit available
	Given Logic 2 Case2 valid EDI
    When I hit the API with username and password with valid EDI for logic2 case2
    Then I should validate following for logic2 case2
		| row | code | severity | message |
    	| 1 | R10.0 | CRITICAL | Diagnosis code requires additional (4th,5th, 6th or 7th ) digit.|

  Scenario: Logic 2 Case3 Require Additional Digit- 5th Digit missing
	Given Logic 2 Case3 valid EDI
    When I hit the API with username and password with valid EDI for logic2 case3
    Then I should validate following for logic2 case3
		| row | code | severity | message |
    	| 1 | R10.1 | CRITICAL | Diagnosis code requires additional (5th, 6th or 7th ) digit.|
		
  Scenario: Logic 2 Case4 Require Additional Digit- 6th Digit missing
	Given Logic 2 Case4 valid EDI
    When I hit the API with username and password with valid EDI for logic2 case4
    Then I should validate following for logic2 case4
		| row | code | severity | message |
    	| 1 | S82.84 | CRITICAL | Diagnosis code requires additional (6th or 7th ) digit.|
		
  Scenario: Logic 2 Case5 Require Additional Digit- 7th Digit missing
	Given Logic 2 Case5 valid EDI
    When I hit the API with username and password with valid EDI for logic2 case5
    Then I should validate following for logic2 case5
		| row | code | severity | message |
    	| 1 | S82.843 | CRITICAL | Diagnosis code requires additional 7th digit.|
		
  Scenario: Logic 2 Case6 Require Additional Digit- All seven digits available
	Given Logic 2 Case6 valid EDI
    When I hit the API with username and password with valid EDI for logic2 case6
    Then I should validate following for logic2 case6
		| row | code | severity | message |
    	| 1 | S82.843H | CRITICAL | Diagnosis code requires additional 7th digit.|
