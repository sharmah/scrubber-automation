Feature: Scrubber General Logic_103 When modifier GY is not appended to CPT 92227 and L33567 applies to the claim.

  Scenario: Logic 103 Case1 When modifier GY is not appended to CPT 92227 and L33567 applies to the claim.
    Given Logic 103 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic103 case1
	Then I should validate following for Logic103 case1
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | NGS considers 92227 to be a screening service and will deny it as non-covered (LCD L33567). Append modifier GY to acknowledge the service is not covered (NGS Article A52861).|
		
  Scenario: Logic 103 Case2 When modifier GY is not appended to CPT 92227 and L33567 applies to the claim and A52861 got expired before DOS.
    Given Logic 103 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic103 case2
	Then I should validate following for Logic103 case2
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | NGS considers 92227 to be a screening service and will deny it as non-covered (LCD L33567). Append modifier GY to acknowledge the service is not covered (NGS Article A52861).|
		
  Scenario: Logic 103 Case3 When modifier GY is appended to CPT 92227 and L33567 applies to the claim.
    Given Logic 103 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic103 case3
	Then I should validate following for Logic103 case3
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | NGS considers 92227 to be a screening service and will deny it as non-covered (LCD L33567). Append modifier GY to acknowledge the service is not covered (NGS Article A52861).|