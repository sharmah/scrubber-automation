Feature: Scrubber General Logic_13 CCI edit col1 and col2 codes can never be reported together.
  
  Scenario: Logic 13 Case1 CCI edit col1 and col2 codes can never be reported together.
	Given Logic 13 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic13 case1
	Then I should validate following for logic13 case1
		| row | code | severity | message |
		| 1 | 64463 | CRITICAL | CCI edit: This procedure code can never be reported together with code 40510 due to CCI column 2 code edit|
  
  Scenario: Logic 13 Case2  Codes can be reported together.
	Given Logic 13 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic13 case2
	Then I should validate following for logic13 case2
		| row | code | severity | message |
		| 1 | 62200 | CRITICAL | CCI edit: This procedure code can never be reported together with code 40510 due to CCI column 2 code edit|