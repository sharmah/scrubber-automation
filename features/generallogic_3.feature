Feature: Scrubber General Logic_3 Diagnosis code and gender conflict
  
  Scenario: Logic 3 Case1 Seconday diagnosis code conflicts with gender specified 'M'
	Given Logic 3 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case1
	Then I should validate following for logic3 case1
		| row | code | severity | message |
		| 1 | C50.919 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case2 Primary diagnosis code conflicts with gender specified 'M'
	Given Logic 3 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case2
	Then I should validate following for logic3 case2
		| row | code | severity | message |
		| 1 | Z37.9 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case3 Gender entered is as per diagnosis code.
	Given Logic 3 Case3 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case3
	Then I should validate following for logic3 case3
		| row | code | severity | message |
		| 1 | N46.9 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case4 Primary diagnosis code conflicts with gender specified 'F'
	Given Logic 3 Case4 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case4
	Then I should validate following for logic3 case4
		| row | code | severity | message |
		| 1 | N46.9 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case5 Secondary diagnosis code conflicts with gender specified 'F'
	Given Logic 3 Case5 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case5
	Then I should validate following for logic3 case5
		| row | code | severity | message |
		| 1 | N46.9 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case6 Gender entered is as per diagnosis code.
	Given Logic 3 Case6 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case6
	Then I should validate following for logic3 case6
		| row | code | severity | message |
		| 1 | K40.90 | CRITICAL | Diagnosis and sex conflict.|
   
   Scenario: Logic 3 Case7 Gender entered is as per diagnosis code.
	Given Logic 3 Case7 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case7
	Then I should validate following for logic3 case7
		| row | code | severity | message |
		| 1 | N46.9 | CRITICAL | Diagnosis and sex conflict.|
		
  Scenario: Logic 3 Case8 Gender entered is blank.
	Given Logic 3 Case8 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case8
	Then I should validate following for logic3 case8
		| row | code | severity | message |
		| 1 | N46.9 | CRITICAL | Diagnosis and sex conflict.|

   Scenario: Logic 3 Case9 Diagnosis code conflicts with gender specified 'X'
	Given Logic 3 Case9 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case9
	Then I should validate following for logic3 case9
		| row | code | severity | message |
		| 1 | N46.9 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case10 Diagnosis code conflicts with gender specified '2'
	Given Logic 3 Case10 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case10
	Then I should validate following for logic3 case10
		| row | code | severity | message |
		| 1 | Z37.9 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case11 Diagnosis code conflicts with gender specified 'U'
	Given Logic 3 Case11 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case11
	Then I should validate following for logic3 case11
		| row | code | severity | message |
		| 1 | Z37.9 | CRITICAL | Diagnosis and sex conflict.|
		
   Scenario: Logic 3 Case12 Diagnosis code conflicts with gender specified 'U'
	Given Logic 3 Case12 valid EDI
	When I hit the API with username and password with valid EDI for logic3 case12
	Then I should validate following for logic3 case12
		| row | code | severity | message |
		| 1 | N46.9 | CRITICAL | Diagnosis and sex conflict.|