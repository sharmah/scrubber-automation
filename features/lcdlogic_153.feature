Feature: Scrubber General Logic_153 When LCD L34760 applies to the claim and user enters <code 1>, which is either 92133-26 or 92134-26  and POS <#> is NOT 11, 21, 22, 31, 32, or 49

  Scenario: Logic 153 Case1 When POS entered is 19.
    Given Logic 153 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic153 case1
	Then I should validate following for Logic153 case1
		| row | code | severity | message |
		| 1 | 19 | CRITICAL | Confirm POS. WPS does not list POS 19 as a payable POS for 92133 (Billing and Coding Guidelines attached to LCD L34760).|
		
  Scenario: Logic 153 Case2 When POS entered is 32
    Given Logic 153 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic153 case2
	Then I should validate following for Logic153 case2
		| row | code | severity | message |
		| 1 | 49 | CRITICAL | Confirm POS. WPS does not list POS 49 as a payable POS for 92133 (Billing and Coding Guidelines attached to LCD L34760).|