Feature: Scrubber General Logic_92 when ICD-10 code (group1 and group3) linked to CPT code 92227 and L33467 applies to the claim.

  Scenario: Logic 92 Case1 when E08.36 code (group1 and group3) linked to CPT code 92227 and L33467 applies to the claim.
    Given Logic 92 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic92 case1
	Then I should validate following for Logic92 case1
		| row | code | severity | message |
		| 1 | 92227 | MEDIUM | Policy note: Palmetto indicates 92227 “is not for routine screening, but is covered for evaluation of asymptomatic patients at risk with known disease (e.g. diabetes mellitus) that is likely to cause retinal disease when the test is ordered by the treating physician” (LCD L33467).|
		
  Scenario: Logic 92 Case2 when B25.2 code (group1 and group3) linked to CPT code 92227 and L33467 applies to the claim.
    Given Logic 92 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic92 case2
	Then I should validate following for Logic92 case2
		| row | code | severity | message |
		| 1 | 92227 | MEDIUM | Policy note: Palmetto indicates 92227 “is not for routine screening, but is covered for evaluation of asymptomatic patients at risk with known disease (e.g. diabetes mellitus) that is likely to cause retinal disease when the test is ordered by the treating physician” (LCD L33467).|