Feature: Scrubber General Logic_99 When CPT is 66982 and none of the secondary diagnosis code is from group 2 or no secondary code is entered.

  Scenario: Logic 99 Case1 When only PDx is entered
    Given Logic 99 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic99 case1
	Then I should validate following for Logic99 case1
		| row | code | severity | message |
		| 1 | 92227 | CRITICAL | Use modifier 52 on 92228 to indicate unilateral service (Article A53060). Palmetto does not require modifier RT or LT for a unilateral service.|
		
  Scenario: Logic 99 Case2 When SDx G24.5 (not from group2) is entered.
    Given Logic 99 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic99 case2
	Then I should validate following for Logic99 case2
		| row | code | severity | message |
		| 1 | G24.5 | CRITICAL | Confirm secondary diagnosis code choice. NGS requires an applicable secondary diagnosis code for 66982, but does not list G24.5 as an option (LCD L33538).|
		
  Scenario: Logic 99 Case3 When SDx H22 (from group2) is entered.
    Given Logic 99 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic99 case3
	Then I should validate following for Logic99 case3
		| row | code | severity | message |
		| 1 | H22 | CRITICAL | Confirm secondary diagnosis code choice. NGS requires an applicable secondary diagnosis code for 66982, but does not list H22 as an option (LCD L33538).|
		
  Scenario: Logic 99 Case4 When SDx G24.5 (not from group2) is entered.
    Given Logic 99 Case4 valid EDI
	When I hit the API with username and password with valid EDI for Logic99 case4
	Then I should validate following for Logic99 case4
		| row | code | severity | message |
		| 1 | G24.5 | CRITICAL | Confirm secondary diagnosis code choice. NGS requires an applicable secondary diagnosis code for 66982, but does not list G24.5 as an option (LCD L33538).|