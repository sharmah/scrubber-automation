Feature: Scrubber General Logic_131 when <code 1>92081, 92082, or 92083, LCD L34615 applies to the claim.

  Scenario: Logic 131 Case1 When CPT 92081 is entered
    Given Logic 131 Case1 valid EDI
	When I hit the API with username and password with valid EDI for Logic131 case1
	Then I should validate following for Logic131 case1
		| row | code | severity | message |
		| 1 | 92081 | CRITICAL | Append modifier 26 to report only the professional component of 92081 when performed at POS 21 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 131 Case2 When CPT 92082 is entered with mod 26
    Given Logic 131 Case2 valid EDI
	When I hit the API with username and password with valid EDI for Logic131 case2
	Then I should validate following for Logic131 case2
		| row | code | severity | message |
		| 1 | 92082 | CRITICAL | Append modifier 26 to report only the professional component of 92082 when performed at POS 21 (LCD L34615, Billing and Coding Guidelines attachment).|
		
  Scenario: Logic 131 Case3 When CPT 92083 is entered with mod TC
    Given Logic 131 Case3 valid EDI
	When I hit the API with username and password with valid EDI for Logic131 case3
	Then I should validate following for Logic131 case3
		| row | code | severity | message |
		| 1 | 92083 | CRITICAL | Append modifier 26 to report only the professional component of 92083 when performed at POS 21 (LCD L34615, Billing and Coding Guidelines attachment).|