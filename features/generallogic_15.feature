Feature: Scrubber General Logic_15 CCI does not bundle E/M code with any other code
  
  Scenario: Logic 15 Case1 CCI does not bundle E/M code with any other code
	Given Logic 15 Case1 valid EDI
	When I hit the API with username and password with valid EDI for logic15 case1
	Then I should validate following for logic15 case1
		| row | code | severity | message |
		| 1 | 99203 | LOW | CCI does not bundle 99203 into "60000", which you have submitted for the same DOS. But some payers may require modifier 25 with 99203 to indicate a significant and separately identifiable E/M service|
		
  Scenario: Logic 15 Case2 CCI does not bundle Enum code with any other code. Mod 25 appended to E/M code.
	Given Logic 15 Case2 valid EDI
	When I hit the API with username and password with valid EDI for logic15 case2
	Then I should validate following for logic15 case2
		| row | code | severity | message |
		| 1 | 99203 | LOW | CCI does not bundle 99203 into "60000", which you have submitted for the same DOS. But some payers may require modifier 25 with 99203 to indicate a significant and separately identifiable E/M service|